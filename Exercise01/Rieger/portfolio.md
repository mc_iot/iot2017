# Exercise 1

# Install Arduino IDE on Arch Linux using yaourt:
```sh
yaourt -S arduino
```

Cloning - done by Team Fischbacher/Graf
```sh
sudo ./rpi-clone sda
```

# Change access point name/pw in ulnoiot.conf:

```
# Name of accesspoint to create
ULNOIOT_AP_NAME="iotempire-InternetOfBoys"
ULNOIOT_AP_ADDID=no
# Its pw
ULNOIOT_AP_PASSWORD="internetofboys"
```

# SSH access:
```sh
ssh pi@ulnoiotgw -L 5901:localhost:5901
pw: ulnoiot
```

# Activate VNC server:
```sh
Raspi-config
```
 - Interface options -> VNC
 - RealVNC client
 - Connect with ulnoiotgw:2 after starting vncserver

Connect with  ESP8266 via USB

In Arduino IDE
 - Open Files->Examples->ESP8266 Blink program
 - Choose WeMos R2 & Mini as Board and USB device as Port
 - Change user permission of IDE workspace and upload
 - Blink!
