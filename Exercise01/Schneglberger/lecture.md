# Lecture 01

#Whoami
####$Martin Schneglberger

#What type of programming experience?
Internships as Frontend Developer otherwise all we learned in the University (Java, C, C++, Swift), Kotlin

#What have you done with micro controllers?
Mostly small projects in school (Microchip) and university (Texas Instruments)
(bicylce computer in HNP)

#Single board Computers
- Smart kitchen scale with Raspi 3 and Android Things
- Used Raspberry PI as a smart media center
- Some smaller projects (blinking lights etc.) with Raspi
- Made a Tor node with a raspberry pi
- Hosted a Hidden Service on a raspberry pi

#Who is a maker or part of a maker community?
Unfortunately, I am not part of maker community (but it sounds very interesting)

#What do you already know about IoT?
Important in Industry 4.0, Smart grocery stores, Smart homes
Many little or big devices communicating over a network

#What are your expectations from this class?
- Learn about what can be achieved with the IoT
- Practical examples and small projects
- Also about some common protocols/communication principles
- Detailed information about sensors
- Something about Open Hardware would be great
- Maybe if we have time a little bit insight (can even be theoretical) in operating systems like win10 IoT and Android things…but I do not expect that or see this as a “must-have”


#IoT
**Group work done with Michael Rockenschaub and Dominik Reichinger**
#What does the Internet of Things entail?
##Domains

[http://internetofthingsagenda.techtarget.com/feature/Can-we-expect-the-Internet-of-Things-in-healthcare](http://internetofthingsagenda.techtarget.com/feature/Can-we-expect-the-Internet-of-Things-in-healthcare)
[https://www.computerwoche.de/a/industrie-4-0-ist-das-internet-der-ingenieure,2538117](http://internetofthingsagenda.techtarget.com/feature/Can-we-expect-the-Internet-of-Things-in-healthcare)
[https://www.tecchannel.de/a/internet-of-things-das-neue-tecchannel-compact-ist-da,3277894](http://internetofthingsagenda.techtarget.com/feature/Can-we-expect-the-Internet-of-Things-in-healthcare)

- Healthcare
- Industry 4.0
- Smart Homes
- Smart Car/City
- Asset tracking
- Aircrafts
- Deliverty systems
- M2M communication
- Mobile and Wearable Computing

##Commonly used (data) protocols
https://www.rs-online.com/designspark/eleven-internet-of-things-iot-protocols-you-need-to-know-about

Infrastructure:

- 6LowPAN (IPv6 over Low power Wireless Personal Area Network)
- IPv4/IPv6
- RPL

Transport protocol:

- Bluetooth
- Zigbee
- Z-wave
- Thread
- Wifi
- Ethernet

https://www.postscapes.com/internet-of-things-protocols/

Data protocols:

- MQTT
- CoAP
- AMQP
- Websocket
- Node

##Typical devices

- Intel Edinson
- Raspi
- Arduino Uno
- C.H.I.P
- Mediatek Linkit One
- Particle Photon
- Tessel
- Adafruit Flora
- LightBlue Bean
- Udoo Neo

**<Compared with Michael Rockenschaub and Dominik Reichinger>**

##Benefits of IoT

- Many small devices
- No limiting standards resulting in many possible applications
- Makes some processes faster
- Huge number of connections per person (all can be connected)
##Challenges of IoT:

- Security (Attack on this devices)
- Right platform, Hardware, Software => quite complex
- Who pays for it?
- Connectivity not accepted in all markets
- Fast speed => Power consumption high => battery life go down

**</Compared with Michael Rockenschaub and Dominik Reichinger>**

###Benefits in Healthcare:

- Decreased Costs: When healthcare providers take advantage of the connectivity of the healthcare solutions, patient monitoring can be done on a real time basis, thus significantly cutting down on unnecessary visits by doctors. In particular, home care facilities that are advanced are guaranteed to cut down on hospital stays and re-admissions.  
- Improved Outcomes of Treatment: Connectivity of health care solutions through cloud computing or other virtual infrastructure gives caregivers the ability to access real time information that enables them to make informed decisions as well as offer treatment that is evidence based. This ensures health care provision is timely and treatment outcomes are improved.
- Improved Disease Management: When patients are monitored on a continuous basis and health care providers are able to access real time data, diseases are treated before they get out of hand.
- Reduced Errors: Accurate collection of data, automated workflows combined with data driven decisions are an excellent way of cutting down on waste, reducing system costs and most importantly minimizing on errors.
- Enhanced Patient Experience: The connectivity of the health care system through the internet of things, places emphasis on the needs of the patient. That is, proactive treatments, improved accuracy when it comes to diagnosis, timely intervention by physicians and enhanced treatment outcomes result in accountable care that is highly trusted among patients.
- Enhanced Management of Drugs: Creation as well as management of drugs is a major expense in the healthcare industry. Even then, with IoT processes and devices, it is possible to manage these costs better.

###Security & Data Governance

**Problem:** Companies are concerned, for good reason, that when they connect their physical environment to the cloud or data centers, it becomes at risk of being accessible to the outside world.

**Solution:** A way to face this challenge is to review and strengthen your current IT security infrastructure.  
Then, carefully select your new hardware and software partners by understanding their security profile. Security should be a number one priority when implementing an IoT platform.

###IT and business partnership

**Problem:** This partnership is not always a natural relationship, so in turn comes challenges.

**Solution:** Start by working with smaller integrated projects that revolve around analytics and security, and then work your way up to the larger projects.

###Data and analytics complexity

**Problem:** A common practice that is being seen within companies is that they are sending their sensor data directly to the cloud or datacenter. This is not always the best choice because it can create latency, drive costs, and can open up security risks.

**Solution:** When dealing with large amounts of data, companies like Dell, recommend looking into Gateways, stating that this is a strong architecture choice to improve performance, reduce cost, and improve security.

###Lack of data protocol standards

**Problem:** An ongoing struggle some companies endure is having a consistent global protocol for all of this extra data that is being collected.

**Solution:** An IoT business model typically has three, distinct facets: launch, manage, and monetize. Set rate plans while integrating your new IoT business with your existing infrastructure. Monitor your connected devices in real-time, tracking data, usage, connectivity, etc. Run diagnostics to identify and troubleshoot issues on any devices, anywhere, at any time. Monetize by setting rates for each type and level of service you offer and define how those plans will be managed over time, and then automate it.
