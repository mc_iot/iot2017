##Exercise 01 - LAB

##Clone done by Fischbacher and Graf
- cd src/rpi-clone
- sudo ./rpi-clone -v sda

##Search for possible VNC (Virtual Network Computing):
- RealVNC (used)
- OR Tight VNC

(Research on reStructuredText and Markdown)

##Connect to raspberry pi
- Connect to Wlan "iotempire-InternetOfBoys"
- ssh pi@ulnoiotgw -L 5901:localhost:5901
    pw: ulnoiot
    -L Tunnel Port from raspberry (localhost:5901) to own computer (5901)

##Setting up VNC
Activate VNC Server on raspberry pi
    raspi-config
      Interface options -> VNC

When connecting in RealVNC ulnoiot:2 name IotBoys after executing vncserver

##Setting up Arduino IDE and chose default blink program
- Connect to ESP8266 with Arduino IDE via USB
- Chose right Board (Wemos DI mini) and port USB0
- Chose blink program from File -> Examplex -> Basic -> Blink
- In order to be able to compile, privilegs have to be set by executing
  sudo chmod -R 755 apps/

##Blink remotely (Martin Schneglberger)
- Open example "Hello Server"
- Edit constants in a way that they contain the credentials for the AP
```
const char* ssid = "iotempire-InternetOfBoys";
const char* password = "internetofboys";
```
- Test the functionality by connecting to the URL printed in the serial plotter either with **curl** or with a browser
- Picture:
  ![alt text](https://bytebucket.org/mc_iot/iot2017/raw/b02dc76ab486c0dabdfb428c402c97d230bd1326/Exercise01/Schneglberger/serial_monitor.png)
- Create a global **toogleLED** flag
- Change the **handleRoot()** in a way to toogle the LED and to send the desired response
```
  void handleRoot(){
    digitalWrite(LED_BUILTIN, toogleLED);
    server.send(200, "text/html","<html><head><title>IOT Boys</title></head><body>Hello World! Greets, your beloved <b>IoT Boys</b>. Our love is <blink>blinking</blink> like the LED</body></html>")
  }
```
- In order to determine the built in LED automatically and configure it as an output, the pinMode(LED_BUILTIN, OUTPUT) line should be added int the **void setup(void)** function.
- Result shown in browser:
  ![screenshot](https://bytebucket.org/mc_iot/iot2017/raw/b02dc76ab486c0dabdfb428c402c97d230bd1326/Exercise01/Schneglberger/blink.png)

##Reasons why lab was Internet of Things:
Two independed devices where connected to each other and could communicate (two-way as there is a optical answer (and an answer in form of a webpage))
