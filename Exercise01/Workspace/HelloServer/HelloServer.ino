#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

/*
 * This program was developed by the IoT Boys.
 * It toogles the built-in LED by sending a request to the IP printed to the Serial Monitor.
 * This can be done with any program / command offering HTTP request functionality.
 */
const char* ssid = "iotempire-InternetOfBoys";
const char* password = "internetofboys";

ESP8266WebServer server(80);

bool toogleLED = false;

void handleRoot() {
  digitalWrite(LED_BUILTIN, toogleLED);
  server.send(200, "text/html", "<html><head><title>IOT BOYS</title></head><body>hello world! Greets, your beloved <b>IoT Boys</b>!!<br/> Our love is <blink>blinking</blink> like the led</body></html>");
  toogleLED = !toogleLED;
}

void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void){
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, toogleLED);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/inline", [](){
    server.send(200, "text/plain", "this works as well");
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
}
