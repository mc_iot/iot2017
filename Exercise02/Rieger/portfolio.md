# Exercise 2

## Run Basic HTTP-Client sample
  - In Arduino IDE open the HTTP Client Example
    - File -> Examples -> ESP8266HttpClient -> BasicHttpClient

### Fill in Access Point information in BasicHttpClient file
```c
WifiMulti.addAP("iotempire-InternetOfBoys", "internetofboys");
```

### Change URL to connect to
```c
http.begin("http://www.example.com/");
```
(notice the slash at the end, had an error because of forgetting it)


## Webhook online configuration
At first done by Martin Schneglberger (see folder Exercise02/Schneglberger/portfolio.md)

Step by step (additionally done by myself):
  - Register at the IFTTT website
  - Create new Applet
    - provide an event name
    - choose "Webhooks" for "this"
    - choose "Notifications" for that
    - finish
  - Click on the Webhooks icon
  - Click on the settings->services
  - Take the URL provided to fire events

## Fire the event from the Arduino
Change the url of the BasicHttpClient to request the Webhook URL
```c
http.begin("https://maker.ifttt.com/trigger/{event}/with/key/bMKgT440NttzLCxnvlbW2j3jNYWFGfB7pSZYAYYdUH0/");
```

## I²C LCD

 - download I²C LCD library for the ESP8266 from https://github.com/agnunez/ESP8266-I2C-LCD1602
  - unzip it and copy it to the Arduino libraries folder
  - open example and inform about usage


include library and initialize the display:

```c
#include <LiquidCrystal_I2C.h>
#include <Wire.h>

LiquidCrystal_I2C lcd(0x27, 16, 2); // address of LCD

void setup() {
  ...
  lcd.begin(0, 4);      // SDA = Pin 0, SCL = Pin 4
  lcd.backlight();
  ...
}

void loop() {
  ...
  lcd.clear();
  lcd.print("Hier könnte ihre Werbung stehen");
  ...
}
```
