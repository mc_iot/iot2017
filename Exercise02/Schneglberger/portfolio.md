#Exercise 02 - LAB

##AAANNND first problem:

git bash ssh could not resolve pi@ulnoiotgw   
**Solution:** Use ubuntu

##AAAANNND second problem:
Ethernet Port in our row in SRMC was not connected   
**Solution:** Plug in raspi in other row (obviously)

##Connect:

$ssh pi@ulnoiotgw -L 5091:localhost:5091  
pw: ulnoiot  
$vncserver  

##Set up in VNC Viewer:

New connection:
- VNC Server: *insert name which was logged after vncserver*
- Name: any

Password: ulnoiot

##Let's get it started

- Connect button (3.3V, GND and D1 for data)

- Select example (File -> Example -> ESP8266HTTPClient -> BasicHTTPClient)

- Connect to WiFi via:
``
WiFiMulti.addAP("iotempire-InternetOfBoys", "internetofboys");
``
- Result from HTTP Post request is printed as string via:
``USE_SERIAL.println(payload.getString())``

###Create Webhook with ITFFF

1. New Applet
2. As *this*, we used a *Webhook* (Trigger: Receive a Web request)
3. As Event name, we chose "button_pressed"
4. As *that*, we used *Notifications* (Notification text: "Halon IotBoys")
5. Go to MyApplets, Select Webhook  
  **Attention:** To get the URL for the webhook, click on the webhook symbol and then go to settings. You will not get the URL if you click on the settings icon of the applet.
6. Download the ITFFF-App on your smartphone, log in with the same account as used on the web app
7. Visit the url (in our case: https://maker.ifttt.com/use/dW_oVaiZuJH0UrJR37W-Qh) with the browser
8. Use the provided URL and description
9. Test the functionality with ``curl -X POST https://maker.ifttt.com/trigger/event/with/key/dW_oVaiZuJH0UrJR37W-Qh
``
**Mistake:** We did not used 'button_pressed' as event and wondered why it did not work...(Dumb, I know...)
10. Replace the  the URL (with the right event ;) ) in the program with one for the webhook...or better (code reformat done by mike):
```
  void loop(){
    sendHttpPost(webhookUrlMartin);

    delay(20000);
  }

  void sendHttpPost(char* url){
    if(WifiMulti.run() == WL_CONNECTED){
      HTTPClient http;

      http.begin(url);
      int httpCode = http.GET();

      http.end();
    }
  }
```
11. Feel the pleasure of getting a notifications every 20 seconds;

##Making same stuff with button

Add condition:
```
  if(digitalRead(5) == LOW){
    sendHttpPost(webhookUrlMartin);
  }
```

##I2C Display
started together, but finished by Michael Rieger see[Exercise02/Rieger/portfolio.md](Exercise02/Rieger/portfolio.md)


##User Scenario 01

Smoky - Guys

A nursing room has hundrets of rooms with each containing 1-3 eldery people.   
Each room should contain a smoke sensor in the main room, a rain/water sensor in the tiles in the bathroom and a weighting sensor under the feet of the bed.  
The sensors of each room can be monitored in the lobby via a laptop  

Nurse: Lilly, Residents: Dominik, Michael and Patricia, Fireman: Uwe

*Lilly* is working shifts in the nursing home in Linz, in which *Dominik, Michael and Patrica* live.  
Patricia lights her Advent wreath every day because she like to read christmas stories at night. Sometimes she forgets to extinguish the candles again, and so it happens that the wreath catches fire. *Lilly* now immediately gets a notification on her laptop that she should go to Patritias room. If she does not respond to that alarm within two minutes, *Uwe* gets called to the place. *Uwe* can then look which sensors report smoke and can specify the area of the fire.  
When *Michael* wants to take a bath after an exhausting game of bingo, he tends to fall asleep while waiting for the bathtube to fill with water. In this case, the water sensors report to *Lilly*.  
*Dominik* tends to run away from the nursing home at night and also walks in his sleep. As soon as he leaves the bed for longer than half an hour at night, the weighting sensors under the bed feet take notice and *Lilly* gets notified about his absence.

##User Scenario 02

A house has multiple windows and is in an area where the weather changes frequently.  
Each room-door and window is supposed to have a smartlock, while there is a rain sensor mounted on the roof of the house and a camera in every room. The windows are additionally equipped with multiple light barriers.   

Landlord: Michael, Thief: Patricia, Neighbor: Dominik, Policeman: Martin

*Michael* has a house in Maria Schmolln, a small city which can suffer from frequent changing weather in summers. Nevertheless, he wants to ventilate his rooms while he is not at home even if there is a bad weather forecast. Due to the fact that he is into electronics he has a lot of circuits and devices which are sensible for water and high humidity laying around in his rooms. To prevent his things from damage, all the windows close automatically if the weather changes to rain.  
The crime-rate in his town is quite high and *Patricia* tries to jump in trough an open window and steal his high-end equipment. The light-barriers notice that move and trigger a locking of the doors and windows of this room and therefore lock in *Patricia*. Also, the camera in this room starts to record everything happening.  
*Michael* immediately get a alarm notification and can see what is currently happening in his beloved home. Due to the 360 degree camera, *Patricia* has no chance to hide from him.  
If Michael does not response within two minutes, the alarm is forwarded to either *Dominik* or *Martin* - depending on the settings Michael chose when leaving his house. As he chose Dominik and he also does not response within two minutes, *Martin* can take insight into the incidence remotely. He can not immediately identify Patricia, so he gets navigated to the GPS location of the house and receives a detailed room plan. When he reads this room he guesses that Patricia hides beneath the bed. As he is absolutelly right, he arrests her and *Michael* gets an email asking for a PayPal donation, which is accepted automatically.
