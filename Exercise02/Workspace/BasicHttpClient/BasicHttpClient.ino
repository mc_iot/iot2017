/**
 * BasicHTTPClient.ino
 *
 *  Created on: 24.05.2015
 *
 */

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

#include <LiquidCrystal_I2C.h>
#include <Wire.h> 

#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;

char* webhookUrlMartin = {"http://maker.ifttt.com/trigger/button_pressed/with/key/dW_oVaiZuJH0UrJR37W-Qh/"};
char* webhookUrlMike = {"http://maker.ifttt.com/trigger/button_pressed/with/key/bMKgT440NttzLCxnvlbW2j3jNYWFGfB7pSZYAYYdUH0/"};

LiquidCrystal_I2C lcd(0x27,16,2); // Check I2C address of LCD, normally 0x27 or 0x3F

void setup() {

    USE_SERIAL.begin(115200);
   // USE_SERIAL.setDebugOutput(true);
   
    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

    for(uint8_t t = 4; t > 0; t--) {
        USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
        USE_SERIAL.flush();
        delay(1000);
    }

    WiFiMulti.addAP("iotempire-InternetOfBoys", "internetofboys");
    
    lcd.begin(0,4);      // SDA=0, SCL=4               
    lcd.backlight();
}

void loop() {
  int val = digitalRead(5);

  if(val == LOW){
    sendHttpPost(webhookUrlMike);
  }

  delay(100);
}

void sendHttpPost(char* url) {
  // wait for WiFi connection
    if((WiFiMulti.run() == WL_CONNECTED)) {

        HTTPClient http;

        USE_SERIAL.print("[HTTP] begin...\n");
        http.begin(url); //HTTP

        USE_SERIAL.print("[HTTP] GET...\n");
        // start connection and send HTTP header
        int httpCode = http.GET();

        if(httpCode > 0) {
            // HTTP header has been sent and Server response header has been handled
            USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);

            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
                USE_SERIAL.println(payload);
                lcd.clear();
                delay(500);
                lcd.home();
                lcd.print("button_pressed<3");
                delay(500);
				lcd.clear();
            }
        } else {
            USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }
        
        http.end();
    }
}

