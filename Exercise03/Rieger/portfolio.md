# Exercise 3

## CoAP

  - Download the CoAP library from GitHub https://github.com/automote/ESP-CoAP
  - unzip and copy it to the Arduino IDE libraries folder
  - open example and flash it
  - connect via Firefox 55 to `coap://192.168.12.100/light`
  - by sending 1 or 0 the light should enable/disable
  - Firefox connection via the Copper extension made huge problems because of the versions (firefox updates itself and makes it hard to not update) -> this cost us a lot of time
## MQTT simulator

  - Download a MQTT library for Java from https://github.com/fusesource/mqtt-client or import via Maven/Gradle

The API is used as follows:

```java
// connect via a blocking call
MQTT mqtt = new MQTT();
mqtt.setHost("192.168.12.1", 1883); // your IP/Port
BlockingConnection connection = mqtt.blockingConnection();
connection.connect();

// publish values under a certain topic
connection.publish("temp", BigInteger.valueOf(22).toByteArray(), QoS.AT_LEAST_ONCE, false);

// subscribe to a certain topic and retrieve messages
connectionToBroker.subscribe(topics);
Message message = connectionToBroker.receive();
byte[] payload = message.getPayload();
int temp = new BigInteger(payload).intValue();
```

With this API an Observable (Publisher) and an Observer (Subscriber) were implemented to use this mechanisms for exchanging simulated temperature data. (done by Martin Schneglberger)
