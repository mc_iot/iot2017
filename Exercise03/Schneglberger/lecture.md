# Lecture 03 Martin Schneglberger

Team with Reichinger, Rockenschaub, Felbauer

##Data Formats

###JSON
**J**ava**Script****O**bject****N**otation   
Compact data format which is *human-readable* and is used for data exchange between applications.   
Each document should be valid JavaScript and interpretable with **eval()**.   
Advantage: Parser for every programming language.   
JSON produces less overhead than XML and more readable and writeable (User-Friendly!).   

User-friendly: Easy to read and write without any helpers. Parser for nearly every programming language, can be written by oneself easily also.
Speed: Less overhead than XML.

With JSONP (JSON with Padding) JSON data get embeeded with a function call => data can be transfered across domain borders.

###Binary (CBOR)

Extremly small code size, small message size and extensibility.   
Based on JSON data model.   
No schema needed for data transfer. (no casts)   
Can be encoded in binary => saves bulk and allows faster processing  

###Comparison
Text and binary lose a lot of information as there is no structuring possible (like e.g. address can be seen at one look).   
JSON vs XML => Soap Service, one channel for e.g. firstname...no type needed, so JSON saves Overhead   
Advantage Text: No packing needed   

**EVERY JSON IS ALSO JAML**

**Binary=>Big/Little Endian problem!**
High precision sensor with 1000 values per second => binary as it is faster

##Who does IoT?

###AllSeen Alliance
####I worked on this

Merged with Open Connecitivity Foundation

AllJoyn framework:    
AllJoyn is an open source software framework that makes it easy for devices and apps to discover and communicate with each other. Developers can write applications for interoperability regardless of transport layer, manufacturer, and without the need for Internet access. The software has been and will continue to be openly available for developers to download, and runs on popular platforms such as Linux and Linux-based Android, iOS, and Windows, including many other lightweight real-time operating systems.

The AllSeen Alliance is dedicated to the widespread adoption of products, systems and services that support the Internet of Things with AllJoyn®, a universal development framework.

Premier Members:

- Canon
- LG
- Microsoft
- Qualcomm

Community:

- cloudofthings
- panasonic
- netgear

Ecosystem:


The Alliance's Working Groups and Projects are meritocratic and follow open source best practices to produce code within the scope of the policies set by the Technical Steering Committee. Projects are proposed to and approved by the Technical Steering Committee. Every project has one or more committers. These project committers make decisions within the Working Group, and elect the Working Group Chair as a leader to represent the Working Group on the Technical Steering Committee.

###OCF (Open Connectivity Foundation)

Members:

- Intel
- Microsoft
- Cisco
- Canon
- LG
- Qualcomm
- Samsung
- CableLabs
- Electrolux
- Haier

Founders:   
Samsung, Intel

Purpose:   
OCF delivers a framework that enables easy discovery, and trusted and reliable connectivity between things in a network via a specification and a certification program.

###ThreadGroup
Thread Group

founded by ARM, Samsung and Nest

Connect and control IoT devices at home.   
Providing a Thread which connects all devices at home   
CD

http://threadgroup.org/joinus

=features=

Simple for consumers to use   
Always secure   
Power-efficient   
An open protocol that carries IPv6 natively   
Based on a robust mesh network with no single point of failure   
Runs over standard 802.15.4 radios   
Designed to support a wide variety of products for the home: appliances, access control, climate control, energy management, lighting, safety, and security   

IIC

Title: Industrial Internet Consortium   
Notable Members: Intel, IBM, GE, Cisco, AT&T (founded in 2014) and Bosch, Huawei, SAP   
Purpose: setting the architectural framework for industrial IoT - making it easier to adopt interconnected machines and intelligent analysis.  
Cost: from $2500 to $150.000 in six levels of membership

##Protocols

###MQTT

MQTT (Message Queue Telemetry Transport)
very lightweight

Example:

- Facebook Messenger
- Amazon Web Services announced Amazon IoT based on MQTT
- Microsoft Azure IoT Hub uses MQTT as its main protocol for telemetry messages

Challenges:  
The publish-subscribe messaging pattern requires a message broker. The broker is responsible for distributing messages to interested clients based on the topic of a message.

Software support:    
Zigbee   
Built in security and authentication

###CoAP

UDP instead of TCP (Datagrams)   
publish-subscribe => based on the rest model   
Compared to HTTP.Post HTTP.GET => REST over UDP (Advantage less latency)
