# Exercise 03 lab

- Link for CoAP library: https://github.com/automote/ESP-CoAP
- clone on Raspberry Pi with git clone <link>
- open example and flash it
- connect via Firefox 55 to `coap://192.168.12.100/light`
- by sending 1 or 0 the light should enable/disable

Mistakes:

- cloned into home directory => Arduino IDE did not have the right permissions to access the header files in there
- Moved to examples
- Needed some time to find Firefox 55
- Installed copper plugin
- Needed to restart Firefox in order for plugin to get active
- Firefox had latest version again

MQTT Library: [library](https://pubsubclient.knolleary.net/)
I created a new Java project containing both extecutable classes: the Publisher and the Subscriber.

While the subscriber is listening for the temperature and switch topics, the publisher constantly sends the temperature and a switch signal whenever the temperature reaches a certain degree.   
The publisher has a subclass which simulates the sensor values and sends them to the broker.

Most important methods concering connection:

```
public static BlockingConnection connectToServer() throws Exception {
        MQTT mqtt = new MQTT();
        mqtt.setHost("192.168.12.1", 1883);
        BlockingConnection connection = mqtt.blockingConnection();
        connection.connect();
        return connection;
}
```
Sending the temperature:
```
connectionToBroker.publish(MqttHelper.TEMPERATURE_TOPIC, BigInteger.valueOf(degree).toByteArray(), QoS.AT_LEAST_ONCE, false);
```
Creating an Publisher and Subscriber with this API was done by me.

Install node red

- sudo npm install -g node-red
- node-red
- Open http://localhost:1880
