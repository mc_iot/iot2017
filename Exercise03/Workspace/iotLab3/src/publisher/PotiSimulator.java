package publisher;

import org.fusesource.mqtt.client.QoS;
import util.MqttHelper;

import java.util.Scanner;

/**
 * Created by Martin on 16.10.2017.
 */
public class PotiSimulator extends Publisher {

    private static int currentTemperature = 20;
    private static Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {

        String input;

        try {
            connectionToBroker = MqttHelper.connectToServer();
        } catch (Exception _e) {
            System.out.println("Connection failed");
        }

        System.out.println("Decrease (--) or Increase (++) ");
        while (true) {
            input = reader.nextLine();

            if(input.equals("++")){
                currentTemperature++;
                sendNewPotiValue();
            } else if(input.equals("--")){
                currentTemperature--;
                sendNewPotiValue();
            } else {
                System.out.println("Only ++ and -- allowed as input");
            }
            System.out.println("Decrease (--) or Increase (++) ");
        }

    }

    private static void sendNewPotiValue(){
        try {
            connectionToBroker.publish(MqttHelper.POTI_TOPIC, String.valueOf(currentTemperature).getBytes(), QoS.AT_LEAST_ONCE, false);
        } catch (Exception _e) {
            System.out.println("Publishing Poti value failed");
        }
        System.out.println("New value sent: "+currentTemperature);
    }

}
