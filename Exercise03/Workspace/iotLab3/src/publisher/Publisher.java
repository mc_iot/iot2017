package publisher;

import org.fusesource.mqtt.client.*;
import util.MqttHelper;

public class Publisher{

    static BlockingConnection connectionToBroker;

    Publisher(){
        try {
            connectionToBroker = MqttHelper.connectToServer();
        } catch (Exception _e) {
            _e.printStackTrace();
            System.out.println("Connection to MqttBroker failed");
        }
    }
}
