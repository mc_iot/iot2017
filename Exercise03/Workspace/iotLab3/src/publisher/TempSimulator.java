package publisher;

import org.fusesource.mqtt.client.QoS;
import util.MqttHelper;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Martin on 16.10.2017.
 */
public class TempSimulator extends Publisher implements Runnable{

    private int maxTempInDegree = 15;

    public static void main(String[] args) {
        Thread temperatureThread = new Thread(new TempSimulator());

        while (true) {
            temperatureThread.run();
        }
    }

    @Override
    public void run() {
        try {
            simulateTemp();
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println("Error while simulating temperature");
        }
    }

    private void simulateTemp() throws Exception {
        int degrees = ThreadLocalRandom.current().nextInt(maxTempInDegree - 2, maxTempInDegree);
        if (maxTempInDegree >= 30) {
            maxTempInDegree = 15;
        } else {
            maxTempInDegree++;
        }
        connectionToBroker.publish(MqttHelper.TEMPERATURE_TOPIC, String.valueOf(degrees).getBytes()/*BigInteger.valueOf(degrees).toByteArray()*/, QoS.AT_LEAST_ONCE, false);
        System.out.println("Published temp: " + degrees);
    }
}
