package subscriber;

import org.fusesource.mqtt.client.*;
import util.MqttHelper;

import java.math.BigInteger;
import java.net.URISyntaxException;

public class Subscriber {

    public static void main(String[] args) {

        BlockingConnection connectionToBroker;

        try {
            connectionToBroker = MqttHelper.connectToServer();
        } catch (URISyntaxException e) {
            System.out.println("Illegal URL");
            return;
        } catch (Exception e) {
            System.out.println("Connection failed");
            return;
        }

        Topic[] topics = {new Topic(MqttHelper.TEMPERATURE_TOPIC, QoS.AT_LEAST_ONCE),
                new Topic(MqttHelper.SWITCH_TOPIC, QoS.AT_LEAST_ONCE)};

        while (true) {
            try {
                connectionToBroker.subscribe(topics);
                Message message = connectionToBroker.receive();
                switch (message.getTopic()) {
                    case MqttHelper.TEMPERATURE_TOPIC: {
                        byte[] payload = message.getPayload();
                        int temp = new BigInteger(payload).intValue();
                        //System.out.print(temp);
                        if (temp > 21) {
                            connectionToBroker.publish(MqttHelper.SWITCH_TOPIC, "on".getBytes(), QoS.AT_LEAST_ONCE, false);
                            System.out.println("A/C on");
                        } else {
                            connectionToBroker.publish(MqttHelper.SWITCH_TOPIC, "off".getBytes(), QoS.AT_LEAST_ONCE, false);
                            System.out.println("A/C off");
                        }
                        break;
                    }
                    default: {
                        //byte[] payload = message.getPayload();
                        //System.out.print(payload[0] != 0);
                        break;
                    }
                }
                message.ack();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
