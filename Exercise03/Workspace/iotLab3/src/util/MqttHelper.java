package util;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;

public class MqttHelper {

    public static final String TEMPERATURE_TOPIC = "temp";
    public static final String SWITCH_TOPIC = "switch";
    public static final String POTI_TOPIC = "temp_adjust";

    public static BlockingConnection connectToServer() throws Exception {
        MQTT mqtt = new MQTT();
        mqtt.setHost("192.168.12.1", 1883);
        BlockingConnection connection = mqtt.blockingConnection();
        connection.connect();
        return connection;
    }
}
