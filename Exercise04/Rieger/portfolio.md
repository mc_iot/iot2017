# Exercise 4

## Node-RED
  - downloaded Node-RED via yaourt `yaourt -S node-red`
  - start it by typing `node-red` into the shell
  - open the IP address displayed by the application to access Node-RED
  - to subscribe to the temperature messages a MQTT input is needed
  - for sending the A/C switch message use a MQTT output

to implement the integrator we used a JavaScript function

```js
var returnMsg = {payload: 'off'};

if(msg.payload[0] > 21) {
    returnMsg.payload = 'on';
} else {
    returnMsg.payload = 'off';
}

return returnMsg;
```

## Mongoose OS
Downloaded Mongoose OS from their website but unfortunately it didn't work on our Linux versions. On Arch Linux it crashed immediately after the startup and on Ubuntu it wouldn't continue after the selection of the devices. Colleague switched to Windows 10 and did the rest of this exercise on his notebook.

  - after launching Mongoose OS connect to the Wemos
    - connect the Wemos via USB
    - choose the device in the selection dialog
    - select the firmware to flash for "esp8266"
    - enter WiFi credentials
  - After connection the application was able to
    - toggle the onboard LED
    - read a button input
    - receive QMTT messages and enable/disable the onboard LEDsettings
  - Problems
    - the device always crashed when changing someting in the config
    - had to flash more than one time since errors occured
