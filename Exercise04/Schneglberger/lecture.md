# Lecture 04 Martin Schneglberger

####Group work with Rockenschaub and Reichinger => MQTT and Simulators

Things in *()*  added during the general discussion.

###What will be issues scaling?
- Depends on stresstest results of broker
  - How will the system perform when there are a lot of packages
- IPv4 addressspace is limited
- Priority of calls from publisher
- Intime packages to all subscribers
- (Protect device failure)
- (Remote updating/flashing)

###How can we do testing?
- With nodes that only simulate data
- One simulate Publisher and one Subscriber. With unit tests, expect the certain values
- Stresstesting broker with a lot of publishers sending huge amount of data to only one subscriber.
- Stresstest broker with one publisher and a lot of subscriber (stresstesting)
- Physical testing environment
- (Stories used for finding corner cases)
- (Simulate device failure)

###What role will play?
- MQTT
  - Helps subscribesbs to easily only receive the topics they are interested to
  - (Only one bus => one central point if we only have one broker)
  - Takes control of the publish/subscribe pattern
  - (Single point of failue when not implementing more brokers)
  - (Pretty mature and tested a lot)
- Stories
  - With stories we can describe the problem easily
  - Non-technical level of abstraction
  - Defines the necessary devices and components
  - (Help to communicate between stakeholder)
  - (Used for finding corner - cases / tests)
- Simulator
  - Can be used for used for testing the systems by using defined values
  - Scaling
  - Error handling (e.g. String to topic which is usually used for Integers etc)
  - Mock test cases which are hard to achieve in a real environment (degree: 700°)
  - (No hardware needed to test)

---
####IoT Frameworks

###Node-RED (This i did myself)

Title: Node-RED  node rapid event development
Public available with npm: https://github.com/node-red/node-red (Apache 2.0 license.)

**Documentation**  
Really good, a lot of examples in a structured sector on their homepage.   
Also other websites like http://noderedguide.com/ which offer step by step tutorials   
Youtube videos like "Node RED in 5 minutes"  

**Support**
- Raspberry PI
- Arduino (some of them)
- BeagleBone Black
- Android

**Security**  
By default, the Node-RED editor is not secured - anyone who can access the IP address and port it is running on can access the editor and deploy changes. This is only suitable if you are running on a trusted network.   
Nevertheless, there are many ways to make the system secure by authentication, token expiration etc.

**Mass deployment/Maintainance**
If you have a lot of nodes, the GUI gets a mess.

####Node-RED tutorial

- run **node-red**   
- Edit mqtt-broker node
  - Server: localhost
  - IP-Address: raspi
- Define Topic the device is listening to
- Take debug node and add it to the topic (graphicaly)
- Press deploy
- Test this by login in and send via mqtt_send **topic**
- Check if broker receives message
