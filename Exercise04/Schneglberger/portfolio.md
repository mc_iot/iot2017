#Lab 04 Martin Schneglberger

##Node RED
First done by Michael Rieger as he was in the earlier exercise, but I did it again on my own afterwards.
Screenshot:

##Problem MongooseOS
MongooseOS always showed device status: no device address     
Solution: Use Windows     
![alt text](./mongoose_success.PNG)

##MongooseOS
Attention: Differ carefully between var and let variables!   
Another mistake happened by using the wrong method signature for subscribing to MQTT:   
Wrong one:
```
MQTT.sub(topic_node, function(conn, message) {
   print('Got message:', message);
   if(message === 'on'){
      let value = GPIO.write(led, 0);
   } else {
     GPIO.write(led, 1);
   }
}, null);
```
This one always just printed the topic. We found this signature in a forum, but obviously it was deprecated.   
Right one:
```
MQTT.sub(topic_node, function(conn, topic, message) {
   print('Got message:', message);
   if(message === 'on'){
      let value = GPIO.write(led, 0);
   } else {
     GPIO.write(led, 1);
   }
}, null);
```
Another problem was that the device shut down everytime we changed the *conf0.json*.  
We could not do anything then always write our code once more.

Content of *conf0.json* (extraction):
```
"mqtt": {
    "enable": true,
    "server": "192.168.12.1:1883",
    "client_id": "",
    "user": "",
    "pass": "",
    "reconnect_timeout_min": 2.0,
    "reconnect_timeout_max": 60.0,
    "ssl_cert": "",
    "ssl_key": "",
    "ssl_ca_cert": "",
    "ssl_cipher_suites": "",
    "ssl_psk_identity": "",
    "ssl_psk_key": "",
    "clean_session": true,
    "keep_alive": 60,
    "will_topic": "",
    "will_message": ""
  },
```

##Final exercise status:
We used our simulator from exercise 03 for the temperature values, since we had not other ESP8266.    
With Node RED we implemented the logic which publishes a switch event if a certain temperature is reached ("on" to topic **switch_node**). Otherwise "off" is published to that topic.     
When we press the button conntected to the ESP8266, "on" or "off" gets sent to the topic **switch_node**.     
The built-in LED always represents the current switch status (independend from what changed the state)  
