load('api_config.js'); 
load('api_gpio.js'); 
load('api_mqtt.js'); 
load('api_net.js'); 
load('api_sys.js'); 
load('api_timer.js'); 
load('api_neopixel.js'); 
 
// Button pin = D3 
// LED strip pin = D4 
// LCD pins: SDA = D2, SCL = D1 
 
let led = Cfg.get('pins.led'); 
let button = Cfg.get('pins.button'); 
let state_topic = 'state';
let reset_topic = 'reset';

print('LED GPIO:', led, 'button GPIO:', button); 
 
let getInfo = function() { 
  return JSON.stringify({ 
    total_ram: Sys.total_ram(), 
    free_ram: Sys.free_ram() 
  }); 
}; 
 
// Blink built-in LED every second 
//GPIO.set_mode(led, GPIO.MODE_OUTPUT); 
//Timer.set(1000 /* 1 sec */, true /* repeat */, function() { 
//  let value = GPIO.toggle(led); 
//  print(value ? 'Tick' : 'Tock', 'uptime:', Sys.uptime(), getInfo()); 
//}, null); 
 
let pin = 2, numPixels = 8, colorOrder = NeoPixel.GRB; 
let strip = NeoPixel.create(pin, numPixels, colorOrder); 
let count = 0; 
let timerId = -1; 
strip.clear(); 
strip.show(); 
 
let acState = false;

// Publish to MQTT topic on a button press. Button is wired to GPIO pin 0
// Resets the threshold of the AC to a certain amount of degrees

GPIO.set_button_handler(button, GPIO.PULL_UP, GPIO.INT_EDGE_NEG, 200, function() { 
 
  let ok = MQTT.pub(reset_topic, "reset", 1);
  print('Published:', ok, reset_topic, '->', "reset");
  let timerId = Timer.set(100, 1, function() { 
      strip.clear(); 
      strip.setPixel(count++, 255, 255, 0); 
      strip.show();
      if(count==9){
        Timer.del(timerId);
      }
    }, null);
  
}, null); 

 
// Monitor network connectivity. 
Net.setStatusEventHandler(function(ev, arg) { 
  let evs = '???'; 
  if (ev === Net.STATUS_DISCONNECTED) { 
    evs = 'DISCONNECTED'; 
  } else if (ev === Net.STATUS_CONNECTING) { 
    evs = 'CONNECTING'; 
  } else if (ev === Net.STATUS_CONNECTED) { 
    evs = 'CONNECTED'; 
  } else if (ev === Net.STATUS_GOT_IP) { 
    evs = 'GOT_IP'; 
  } 
  print('== Net event:', ev, evs); 
}, null); 
 
 
MQTT.sub(state_topic, function(conn, topic, msg) { 
   if (msg === 'coolDown' ) {
    // if (timerId === -1) {
    //   timerId = startRgbLeds();
    // }
    coolDownLED();
  } else if(msg === 'heatUp'){
    heatUpLED();
  } else {
    //if (timerId !== -1) {
    //  resetRgbLeds(timerId);
    //  timerId = -1;
    //}
    perfectLED();
  } 
}, null);

function startRgbLeds() { 
  let timerId = Timer.set(100, 1, function() { 
      strip.clear(); 
      strip.setPixel(++count % 8 /* pixel */, 0, 30, 0); 
      strip.show(); 
    }, null); 
 
  return timerId; 
}

function heatUpLED(){
  strip.setPixel(1, 255, 0, 0); 
  strip.setPixel(2, 255, 0, 0); 
  strip.setPixel(3, 255, 0, 0); 
  strip.setPixel(4, 255, 0, 0); 
  strip.setPixel(5, 255, 0, 0); 
  strip.setPixel(6, 255, 0, 0); 
  strip.setPixel(7, 255, 0, 0); 
  strip.show();
}

function coolDownLED(){
  strip.setPixel(1, 240, 136, 240); 
  strip.setPixel(2, 240, 136, 240); 
  strip.setPixel(3, 240, 136, 240); 
  strip.setPixel(4, 240, 136, 240); 
  strip.setPixel(5, 240, 136, 240); 
  strip.setPixel(6, 240, 136, 240);
  strip.setPixel(7, 240, 136, 240);  
  strip.show();
}

function perfectLED(){
  strip.setPixel(1, 0, 30, 0); 
  strip.setPixel(2, 0, 30, 0); 
  strip.setPixel(3, 0, 30, 0); 
  strip.setPixel(4, 0, 30, 0); 
  strip.setPixel(5, 0, 30, 0); 
  strip.setPixel(6, 0, 30, 0); 
  strip.setPixel(7, 0, 30, 0); 
  strip.show();  
  strip.show();
}

function resetRgbLeds(timerId) {
  Timer.del(timerId);
}