# Exercise 5

## Flash ulnoiot to the Wemos

  - tried initializing it from the home directory, obviously it didn't work
  - copy the system_template and copy it to the workspace
  - rename it and the node_template folder inside of it
  - change the directory to the node's folder and execute `initialize`

## Lab Work

  - set the wifi configuration `wifi("SSID", "password")`
  - configure the NeoPixel RGB led strip `rgb_multi("acstrip", d2, 8)`
  - run the program with `run()`
  - had big issues there because we didn't figure out how the mqtt topic was really called (it wasn't that hard after thinkng for about 2 seconds)
    - we always added the main topic which was not neccessary
