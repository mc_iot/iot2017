#Lab 05

Update Ulnoiot:

```
- cd ulnoiot
- git pull
- fix_bin
- ulnoiot upgrade
```

Problem:

- `help("button")` did not work
- Was not support to work, since that is a command for micropython (running on ESP)
- Initialize was not executed in the right folder

Generate new project with template from lib/system_template to workspace/ulnoiot/project1  

Rewatching the IoT tutorial

(To be honest, we then implemented a little bit on project 1 since I was on a "Probewochenende")    
(Of course, as a result we had to do more work with Ulnoiot at home instead to compansate this.)
