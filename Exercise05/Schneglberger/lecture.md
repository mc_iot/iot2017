#Lecture 05 - Martin Schneglberger

##Things I like to see:
- Easy to refactor
- Fast flashing / deploying (**yes**)
- Collection of firewares
- Some examples for common projects like MQTT
- Good error-handling (Syntax etc.) (**yes**)
- Good syntax highlighting (**yes**)
- Good documentation (**yes**)
- Code completion (**yes**)
- Go to implementation / definition by simply clicking on classes (**yes**)
- Debugging features (break-points, memory / variable monivtor, Breakpoint Conditions)
- Remote flashing (**yes**)
- Git integration (**yes**)

8/12 =>  66% (but to be fair, not all points where discussed)
See points I marked with yes
