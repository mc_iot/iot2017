# Exercise 6

## Continue project 1 in ulnoiot

We connected the RGB led strip to our Wemos in ulnoiot.

```python
acstrip = rgb_multi("acstrip", d1, 8) # mqtt subtopic acstrip, on pin 1, 8 LEDs
```

With `run()` we started it and the test case was the animation that I received from Ulno.
Test message looked like the following: `mqtt_send ac_node/acstrip/animation "s 1 blue s 2 blue f 1 yellow f 2 red p 4000 e move 8 p 500 e move -8 p 500 r"`

At first nothing worked but I couldn't really find out why. After asking Ulno, he told me that I should try importing the rgb_animator class with `from ulnoiot import _rgb_animator` in the serial console of the MicroPython shell. At first it didn't work but after trying like five times it did what we wanted.

With the combinations we realized the desired animations that should be played when reaching certain temperatures.

![](led-strip.jpg)
