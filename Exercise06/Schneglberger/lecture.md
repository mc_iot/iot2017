#Lecture 06

##Questions
- How can I access the LED strip?
- Is there any easy way to show function parameters and the documentation inside the same console (like beneath/above the current line)?
- Is there any debug possiblity?
- Can the devices - command also list the according pins or maybe even mqtt topics?
- Is their a potiometer example? If yes, how does it work?

##Use joystick
- Start servers (when not using Raspi)
- Controller basically buttons
- mqtt_listen in the example joystick project
- Use PyKeyboard to remotely control keyboard and mouse
- Import PyKeyboard and Integriot: [link to github](https://github.com/ulno/ulnoiot/tree/master/examples/gaming/joystick1)
- Integration done by callbacks (only when *change*)
