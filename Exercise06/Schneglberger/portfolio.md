#Lab 06

###Installing ulnoiot on my Ubuntu
Problem:   
This error message: => Can't find .local/bin/shebang_run.   
Solution: execute bin/fix_bin in ~/ulnoiot  
Run install  

###Setting up button with ulnoiot

Connecting to wifi with `wifi("IoTEmpire-InternetOfBoys", "InternetOfBoys")`   
Configuring LED Strip: `rgb_multi("acstrip", d2, 8)`   
Running: `run()`


`mqtt_send test_node/acstrip/set on` (to test the functionality)

**Mistake**:  
test_node appended automatically, therefore only mqtt_send acstrip/set 2 on    
Yejj, we received this one on our device  

We wasted a lot of time as we always had the wrong topic. The terminal would have shown us the "abosulte" topic, but unfortunately we used mqtt client program which did not show that information and additionally sometimes did not send the mqtt command at all, which made debugging a hell.  

`mqtt_send acstrip/rgb/set 20 20 20` => values behind is red green blue   
`mqtt_send acstript/brightness/set 255` => sets brightness (for rgb(255,255,255))  
