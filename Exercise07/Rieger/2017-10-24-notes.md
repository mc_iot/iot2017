# Lecture notes - 24.10.2017

## Security Measures against the Internet of Broken Things

- What is broken?
  - WPA
  - Automatic updating
  - Single point of failure
  - too many standards
  - device visibility
  - No responsibility
  - devices are too powerful

For security measures a second gateway can be implemented that will be audited

### LORA

#### What is the relation bandwidth/range/power?

- Low power Wide Area Network = LPWAN
- dilemma: can't transmit over wide distances without using a lot of power
- WiFi - high bandwidth, low range
- better range with 2G-5G
- wide area and low power is needed
- LORA
  - not limited by human laws
  - similar to the mobile internet standards
  - 250bps - 50kbps
  - the LORA alliance supports it (Compound of companies)

#### What is the Link budget?

- has to do with the connection between sender/receiver
- is calculated in dB
- is reduced by obstacles in the line of sight or reflection of a signal
- Link budget - 154db
  - higher than the mobile internet
- radio link budget calculators
- 1300km theoretically possible with the link budget of LORA
- signal is consumed by conductive obstacles
- 868MHz frequency in Europe

#### What is the community approach

- LoRaWAN
- different approaches
  - enterprise
    - you pay your network by signing a contract with a LoRa provider
  - community
    - People build private LoRa Gateways
    - those are used by the community to transfer data via LoRa
    - gateways can be used by a lot of devices since one devices can only use 1% of the bandwidth
- LoRa limits the traffic of the IoT devices
- RFN95
- SSX86


#### What are the benefits of LORA?

- new transmission standard between distributed devices and gateways
- ideal for low power sensors distributed everywhere
- high range, low bandwidth

#### What are the problems with LORA?

- LoRa belongs to one company, Semtech
- restricted full duplex

#### After movie

- link budget
  - accounting of all of the gains and losses from the transmitter, through the medium (free space, cable, waveguide, fiber, etc.) to the receiver in a telecommunication system
- radio link budget calculator
  -
- LORA in Austria and Linz
  - microtronics, kapsch and ors comm form an Austria-wide LoRaWAN


![LoRa1](LoRa1.png)


### TTN

https://www.kickstarter.com/projects/419277966/the-things-network

- What is TTN?
  - global, crowdsourced, open, free and decentralized internet of things network.
- It connects sensors and actuators, called "Things", with transceivers called "Things Gateways" to servers called "Things Access".
- The first connection is "Over The Air", the second is "Over The Net". The distributed implementation of these concepts is called "The Things Network".
- Anyone shall be free to set up "Things" and connect to "Things Gateways" that may or may not be their own.
- Anyone shall be free to set up "Things Gateways" and connect to "Things Access" that may or may not be their own. Their "Things Gateways" will give access to all "Things" in a net neutral manner, limited by the maximum available capacity alone.
- Anyone shall be free to set up "Things Access" and allow anonymous connections from the Internet. Their "Things Access" will give access to all "Things Gateways" in a net neutral manner, limited by the maximum available capacity alone. Furthermore their "Things Access" will allow connection of other "Things Access" servers for the distribution of data.
- The "Over The Air" and "Over The Net" networks shall be protocol agnostic, as long as these protocols are not proprietary, open source and free of rights.
- Anyone who perpetrates a "Things Access" or a "Things Gateway" will do so free of charge for all connecting devices and servers.
- Anyone making use of the network is allowed to do so for any reason or cause, possibly limited by local law, fully at own risk and realizing that services are provided "as is" and may be terminated for any reason at any moment. The use may be open for anybody, limited to customers, commercial, not-for-profit, or in any other fashion. "The Things Network" providers will not pose restrictions upon its users.
