# Exercise 7

## Keyestudio weighing scale

- Download an Arduino library for the HX711
- connect the scale system Keyestudio KS0087 DIY Electronics Scale Kit
- Use the example project of the HX711 to read the weight in gramms
- calibrated with scale factor 443.3
- Copy the I2C library of ulnoiot from *ulnoiot/libs/esp8266* and import it in the Arduino IDE
- **Problem:** Tried to flash the example program but the IDE couldn't open the connection
- After booting to Windows 10 flashing was possible


D4 = SCL
D3 = SDA
I2C address = 0x27

## Ulnoiot LED strip animation

```python
r = rgb_multi("r", d1, 10)

mqtt_send ac_node/r/animation "<animation string>"
```

- s = set led number/color
- f = fade goal (e.g. led 1 to led 5)
- p = play time
- e = event/extra
- move = all 10
- r = repeat

```python
# if not working
from ulnoiot import _rgb_animator
gc.mem_free
```
