#Lecture 07 - Martin Schneglberger

###Pick 5 - 8 research papers/articles on the Internet of Broken Things

- https://orcinternational.com/innovation/internet-of-broken-things/
- http://www.zdnet.com/article/the-insecurity-of-the-internet-of-things/
- https://www.aspeninstitute.org/events/internet-broken-things-future-challenges-global-cybersecurity/
- https://hackaday.com/2016/02/08/the-internet-of-broken-things-or-why-am-i-so-cold/
- https://itnews.iu.edu/articles/2016/opinion-how-to-fix-an-internet-of-broken-things.php
- https://www.csmonitor.com/World/Passcode/Passcode-Voices/2016/1026/Opinion-How-to-fix-an-internet-of-broken-things
- https://www.rtinsights.com/iot-interoperability-linthicum/
- https://www.i-scoop.eu/internet-of-things-guide/internet-things-project-failure-success/

What is broken?:
"The inability to test APIs using common approaches and mechanisms.   
The inability to push and pull information from devices using the same interfaces.  
The inability to secure devices using third-party security software.  
The inability to monitor and manage devices using a common management and monitoring layer."


How can it be fixed/counter measures:

"There's a way of developing connected gadgets that aren't easily susceptible to outside attack, that have more security protections, and are designed with security in mind. But it'll take more pressure on industry to make sure that happens."

"We should set standards for IoT devices. One model is the National Institute for Standards and Technology's (NIST) Cybersecurity Framework, along with its work on Cyber-Physical Systems. Over time, these standards could help establish a standard of IoT cybersecurity care, including new approaches to proactive cybersecurity measures."

"The time being policymakers should push flexible, guidance-driven frameworks, not prescriptive regulation. Still, a range of policy options are available to incentivize cybersecurity investments, from tax breaks to public bug bounty programs."

"More attention should be paid to the intersection of IoT and the need to secure supply chains. Since IT systems control everything from phones to factories, ensuring these systems are secure is of vital importance to the global economy. Yet this is a daunting proposition given varying sources of insecurity, from malicious"

"In a new survey from ORC International, 59 percent of respondents said that they believed the IoT would not work because of technical or software malfunctions that people will not know how to repair."

"The mentioned gap between perceptions of IT executives and business executives is already one indicator. It’s pretty obvious that, if there is no alignment on what makes a successful IoT project successful, failure is the consequence. We see exactly the same in digital transformation, information management projects, you name it."

![alt-text](./success.jpg)

Newsworthy failures/successes:

- Nest thermostat was hacked
- Huge DDoS attack in October 2016 which took down many big web sites. IoT devices like webcams were hacked and joined the DDos attack. Aimed at Dyn (major DNS provider) => https://www.welivesecurity.com/2016/10/24/10-things-know-october-21-iot-ddos-attacks/


##Discussion
(I was the moderator)

4 : 9

Pro

- Open source
- Automation is sexy
- Jobs are generated
- Certain regulations from governments could also fix privacy concerns
- (Less working times as work is done quicker - But will this really be the case or will the company just will create more products etc.)


Con:

- Stakeholder have to agree
- Update Problem
- Security problem
- How to store these data sets
- Our homenetworks are not compatible to manage that much devices
- Jobs are lost
- Privacy
- Huge companies providing etc. AC automation will dominate the market, small local companies will loose their customers
