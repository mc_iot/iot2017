# Lab 07
## Scale

The scale keyestudio KS0087 DIY Electronic Scale Kit   
Board is the keyestudio UNO Board  

HX711 Example:  
Sketch -> Include Library manage libaries -> HX711 ADC   
Select Arduino UNO as board  

## Load Calibrate Example

Dout pin: 9    
Sck pin: 10     

*"Try and Error"* thought us that the calibration factor is **443.3** for our scale.  

## Ulno IoT

- run *ulnoiot upgrade*
- Copy *"arduino_ulnoiot_i2c_connector"* from */lib/arduino_ulnoiot_i2c_connector* to Arduino libraries

**Error:**

```
An error occurred while uploading the sketch
error: cannot access /dev/ttyUSB0

error: espcomm_open failed
error: espcomm_upload_mem failed

```
Solution:   
Use Windows :P   
No, seriously, booting to Windows, installing the esp8266 board driver again made uploading possible

```
void setup() {
  Serial.begin(9600);
  Serial.println("Wait...");
  LoadCell.begin();
  long stabilisingtime = 2000; // tare preciscion can be improved by adding a few seconds of stabilising time
  LoadCell.start(stabilisingtime);
  LoadCell.setCalFactor(443.4); // user set calibration factor (float)
  Serial.println("Startup + tare is complete");
}
```

Mistake:   
Tried to run ulnoiot project on esp and make it a I2C-"Client" this way, while running the scale as an I2C "Server"

**To be continued at Lab 08**
