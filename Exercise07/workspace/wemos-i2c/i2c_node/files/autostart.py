# This is the autostart code for a ulnoiot node.
# Configure your devices, sensors and local interaction here.

# Always start with this to make everything from ulnoiot available.
# Therefore, do not delete the following line.
from ulnoiot import *

# The following is just example code, adjust to your needs accordingly.



## Start to transmit every 10 seconds (or when status changed).
# Don't forget (uncomment!) the run-comamnd at the end.
#run(10)
