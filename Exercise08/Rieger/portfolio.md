# Exercise 8

## Keyestudio scale cont'd

- continued setup of ulnoiot on my own notebook
- had an error where ulnoiot could not be executed
  - I entered the path as relative path "~/..." instead of the absolute path "/home/user/..."
- continued setting up the ulnoiot node
  - copied the system_template folder
  - added in autostart.py of node: `scale = i2c_connector("scale", d4, d3)`
  - d4 was the SCL pin and D3 the sda pin
  - connection worked on first try
- problem occured because my ulnoiot was not up to date, so the i2c_connector only delivered zero
- after updating everything worked like a charm


![Keyestudio scale](keyestudio.jpg)


After this we continued our work on the project 2 (see [Project 2](../Project02))
