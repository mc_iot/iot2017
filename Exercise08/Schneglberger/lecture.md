#Lecture 08 - Martin Schneglberger

###What is the relation bandwidth/range/power?
Low Power Wide Area Network = LPWAN

Huge area => Huge power    
Km of reach => no power to spend in IoT => low power limited range  
Bandwidth is directly connected to capacity of channel   

Bandwidth and range:    
Wi-Fi lot of bandwith, low range   
Mobile Internet a lot of bandwith, medium range   
Bluetooth small bandwidth, low range   

But we want low power and wide range => LoRa    
LoRa no WiFi replacement

###What is the Link budget?
You have it at the beginning and spend it over time.    
Attribute on connection between Sender and Receiver   
Charged up by sender and consumend by receiver    
dependent on obstacles (more obstacles worse trasmission)    
Around 154dB    
When buying LoRa => check most used frequencies of your country


###What is the community approach?
LoRa backed by many companies and communities    
The Things Network: Transfere messages between gateway and application => People have to provide gateway    
8 parallel channels = 8 devices   
50% duty cycle = 16 devices   

###What are benefits with LORA?
- Wide Range
- Low Power consumption
- High link budget

###What are problems with LORA?
- Low troughput / Lowe channel capacity
- Max allowed duty cycle per device: 1% (defined by law)
- Smaller bandwith than morse

###Link budget:
A link budget is accounting of all of the gains and losses from the transmitter, through the medium (free space, cable, waveguide, fiber, etc.) to the receiver in a telecommunication system.   
Received Power (dB) = Transmitted Power (dB) + Gains (dB) − Losses (dB)

Earth–Moon–Earth communications:     
Link budgets are important in Earth–Moon–Earth communications. As the albedo of the Moon is very low (maximally 12% but usually closer to 7%), and the path loss over the 770,000 kilometre return distance is extreme (around 250 to 310 dB depending on VHF-UHF band used, modulation format and Doppler shift effects), high power (more than 100 watts) and high-gain antennas (more than 20 dB) must be used.   

In practice, this limits the use of this technique to the spectrum at VHF and above.    
The Moon must be above the horizon in order for EME communications to be possible.

###Radio Link Budget Calculator
Semtech SX1272 (used as Lora antenna)
####WiFi
![alt-text](wifi_1.png)
![alt-text](wifi_2.png)
####LoRa
![alt-text](lora_1.png)
![alt-text](lora_2.png)

###LORA in Austria and Linz
- Austria wide LoRaWan => https://www.microtronics.com/en/technology/lora.html    
- Linz AG have LoraWan in their portfolio: => https://www.linz.at/presse/2017/201702_85574.asp


###What is TTN
The Things Network is building a network for the Internet of Things by creating abundant data connectivity, so applications and businesses can flourish.

The technology they use is called LoRaWAN and it allows for things to talk to the internet without 3G or WiFi. So no WiFi codes and no mobile subscriptions.

It features low battery usage, long range and low bandwidth. Perfect for the internet of things.

###What does it stand for?
The Things Network

###What problems does it solve?
The Things Network is a global, open, crowd-sourced Internet of Things data network.    
The Things Network uses the LoRaWAN network technology to provide low power wireless connectivity over long range.   
###How does it sove them?
The Things Network makes working with LoRaWAN easy with great UX, docs and APIs    
Motivates community to expand the network by setting up gateways
