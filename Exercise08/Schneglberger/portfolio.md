#Lab 08 - Martin Schneglberger

##Exercise 07 cont.
(This time on Michael Rieger's Laptop)   
Had to make ulnoiot work on his Arch Linux at first   

Calibrate Scale again  

```
- help(i2c_connector)
- scale=i2c_connector("scale", d4, d3);
```

We always got some weird values over the I2C bus on Michael's Laptop. We tried to resolve this by using pull-up resistors, which did not work    
=>outdated ulnoiot version    
upgraded and redeployed (now also the counter worked again)    
This took us about a hour      

![alt-text](./scale.jpg)

##Cont. Project 02 (see protocol)
[Project02/Schneglberger/portfolio.md](/Project02/Schneglberger/portfolio.md)
