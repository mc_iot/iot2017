# Lecture notes - 30.10.2017

## Google: small startups business opportunities internet of Things

## Name three implemented successful business ideas
- Adhere Tech: a smart pill bottle that helps patients to take their medications
- IFTTT: helps various apps and devices to work together to produce better results
- BluFlux: deals with advanced antenna, cellular wearable, indoor real time location systems, low power radar, etc.  Google’s Advanced Technologies and Products has selected BluFlux as their design partners

## Name three implemented failed business ideas
https://www.postscapes.com/closed-iot-companies/

  - Narrative (11.68M) - Visual storytelling
  - Zeo (20.64M) - Sleep Monitor
  - SpaceCurve (30.8) - Data analytic software

Main reason for failure:
  - Cisco: 3/4 of all IoT businesses fail because of human factors (people and relationships)

## what are challenges and opportunities arising from IoT for small businesses

### Challenges:

http://metalab.uniten.edu.my/~azie/AllArt/19001%20-%202013%20-%20Oleksiy%20Mazhelis%20-%20Internet-of-Things%20Market,%20Value%20Networks,%20and%20Business%20Models_%20State%20of%20the%20Art%20Report.pdf

Future challenges include potential lack of business cases for extensive use of electronics, lack of desired value for the smart home scenario among the end users/customers, and price sensitivity of the scenario. A great challenge is posed by security issues related to interoperability of the devices. Health concerns related to the long-term exposure to increased amount of radio emissions call for further research and potential regulative actions.

Challenges or risks include that the change of existing traditional production lines to new IoT-adapted lines is very costly. Also, if the technologies used are not standardized, it would cause problems when integrating new machines from other vendors (vendor lock!).

https://link.springer.com/content/pdf/10.1007%2Fs11277-011-0288-5.pdf

The workflows in analyzed enterprise environment, home, office and other smart spaces in
the future will be characterized by cross organization interaction, requiring the operation
of highly dynamic and ad-ho relationships. At present, only a very limited ICT support is
available, and the following key challenges exist

###### Network Foundation
- limitations of the current Internet architecture in terms of mobility, availability, manageability and scalability are some of the major barriers to IoT.

###### Security, Privacy and Trust—in the domain of security the challenges are:
- securing the architecture of IoT– security to be ensured at design time and execution time,
- proactive identification and protection of IoT from arbitrary attacks (e.g. DoS and DDoS attacks) and abuse, and
- proactive identification and protection of IoT from malicious software. In the domain of user privacy, the specific challenges are:
- control over personal information (data privacy) and control over individual’s physical
location and movement (location privacy),
- need for privacy enhancement technologies
and relevant protection laws, and
- standards, methodologies and tools for
identity management of users and objects. In the domain of trust, some of the specific
challenges are:
- Need for easy and natural exchange of critical, protected and sensitive
data—e.g. smart objects will communicate on behalf of users / organizations with
services they can trust, and
- trust has to be a part of the design of IoT and must be
built in.

###### Managing heterogeneity
- managing heterogeneous applications, environments and devices constitute a major challenge.

###### managing:
- large amount of information and mining large volume of data to provide useful services,
- designing an efficient architecture for sensor networking and storage,
- designing mechanisms for sensor data discovery,
- designing sensor data communication protocols sensor data query, publish/subscribe mechanisms,
- developing sensor data stream processing mechanisms,
- sensor data mining—correlation, aggregation filtering techniques design.
- Finally, standardizing heterogeneous technologies, devices, application interfaces etc. will also be a major challenge.

### Opportunities

per-resistance countermeasures, and technology to thwart side-channel attacks. IoT will create new services and new business opportunities for system providers to service the communication demands of potentially tens of billions of devices in future. Following major trends are being observed in use of RFID tags.

Use of ultra low cost tags having very limited features is observed. While the information is centralized on data servers managed by service operators, the value of information resides in the data management operations. Use of low cost tags with enhanced features such as extra memory and sensing capabilities is also observed. The information is distributed both on centralized data servers and tags. The value resides in communication and data management, including processing of data into actionable information. Use of smart fixed or mobile tags and embedded systems is also witnessed.

More functionalities are brought into the tags bringing in local services. For such tags, information is centralized in the tags, while the value resides in the communication management to ensure security and effective synchronization with the network.

Smart devices with enhanced inter-device communication will lead to smart systems, which have high degrees of intelligence and autonomy enabling rapid deployment of IoT applications and creation of new services.

## Three successful business ideas:  
### Big Belly
http://bigbelly.com/ => Smart City Solutions
Create products like a trash compactor bin which sends notifications to the right officials when it is full
### August
http://august.com/ => Smart Locks
Remotely lock and unlock doors in your home, keyless entry for family and friends.  
### Adhere TEch
https://adheretech.com/ => Smart wireless pill bottles
If a patient does not take his/her pills, a notification or alert gets sent to neighbours, family, doctors etc.
### Humavox
http://www.humavox.com/ => Wireless charging of small IoT-devices

## Three failed busniess ideas:  
### Sigmo
https://www.crowdfundinsider.com/2015/05/67907-where-are-they-now-sigmo-fails-to-talk-understand/  
A voice translating device that promised “at the touch of a button” you would be able to instantly translate the words you need into any chosen language and hear the results of your translation.   
Unfortunately for backers of this project the only thing ever delivered were broken promises and pretty pictures.  
Sigmo is a crowdfunding failure.
### Ninja Blocks
https://www.crunchbase.com/organization/ninja-blocks  
Ninja Blocks is the creator of the Ninja Sphere, the missing brain for your smart devices. Ninja Sphere learns from your behaviour, identifies your preferences, and manages your devices so you don't have to.  
The Ninja Sphere incorporates a gesture interface and micro-location technology that accurately locates radios within the home. It is backwards compatible with the Ninja Block, a popular device and platform that supports 100s of third-party devices.  
Ninja's should be effective and invisible as should the interface to your home
Failed:
http://www.linux-magazin.de/NEWS/Ninja-Blocks-Projekt-ist-pleite
Because getting broke while producing devices.
### Narative
https://www.crunchbase.com/organization/narrative  
Narrative provides hardware and software tools for automatic visual storytelling and known for the wearable camera narrative clip.
=>http://getnarrative.com/
### Zeo
https://www.crunchbase.com/organization/zeo  
Zeo manufactures sleep monitors that record electrical activity along the scalp and monitor the user's brain waves.  
Failed: http://www.mobihealthnews.com/22484/take-positive-lessons-from-zeo-failure   
The main problem was the business model, with a suboptimal profit margin; also, users didn't care much about accuracy, and competing devices like the FitBit, based on (less accurate) actigraphy, were apparently "good enough" while also performing other functions like monitoring physical activity.


## Michael Schloh

- Main laboratory in Munich
- started in Seattle
- close to Microsoft
- Attachmate
- did mostly telecom after study
- Freelancer afterwards
- projects
  - partner of Samsung, Seat, Intel
  - Amazon AWS
  - OpenCV
  - WolfSSL
    - SSL replacement for OpenSSL
    - intended to work on low power platforms
  - FreeSwitch
    - VoIP company
    - maintain a level of future proofing for the legacy projects
  - Monero
    - Crypto currency
    - working full time on it
    - **Wien, Dezember - Monero, privacy-based technologies**
    - blockchains can be used in non-monetary ways
    - Michael is leading the hardware team
    - hardware wallet
