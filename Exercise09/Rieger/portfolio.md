# Exercise 09

In this exercise our project 3 team came up with an awesome idea: the Sanitary Supervision Center.
We outlined the project as can be seen in the Bitbucket repository of the Team consisting of Matthias Fischbacher, Lorenz Graf and Patrick Felbauer (https://bitbucket.org/mattfisch34/iot_course_team/src/0a1538f3bc6832e3dc29e31b7ea661a9bb6e3cfd/projects/project3/project_03_scenarios.md?at=master&fileviewer=file-view-default) and explained it to you.

Afterwards Martin and I talked with you about our results of project 2 which can be found here: [Project02/Rieger/portfolio.md](Project02/Rieger/portfolio.md)   
