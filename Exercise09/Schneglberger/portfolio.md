#Lab 09

Today, Michael and I showed you the outcome of our second project!   
To renew your memories: Here is a link to this project: [Project02/Schneglberger/portfolio.md](/Project02/Schneglberger/portfolio.md)   

For the next project, we (Patrick Felbauer, Matthias Fischbacher, Lorenz Graf, Michael Rieger and I) want to make living in a shared flat easier by achieving the following goels:  

- Cross-platform App which showes the data gathered by the various components
- Always see if the toilet is occupied or not  
- Count the remaining toilet paper rolls  
- See who showers for how long
- See the collected data in a RaspberryPi controlled smart mirror  
- PHP and mySQL backend to store all data for a shared flat persistently

See more in the project - portfolio     
Total Project 03 documentation:   
[Project03](https://bitbucket.org/mattfisch34/iot_course_team/src/5351286971057fe7dbe49d4451ddf9bc9743ab8e/projects/project3/?at=master)
