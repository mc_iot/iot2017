# Exercise 10

On this day Lorenz introduced me and my teammates to the Ionic framework that we needed for our SSC app.

We did the following:
- Set up an Ionic project
- learned the basics
- talked a little bit with UlNo
- had fun
- played a little bit with UI components and how projects are structured
- played a little bit with TypeScript

For the work with Ionic I used Visual Studio Code with some Ionic Plugins (Syntax highlighting as well as preview)

(No lecture proof on this day since we only had an exercise)
