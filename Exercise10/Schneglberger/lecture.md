#Lecture 10 - Martin Schneglberger

I found:  

- http://www.businessinsider.de/internet-of-things-small-business-opportunities-platforms-2016-8?r=US&IR=T
- https://www.cio.com/article/2602467/consumer-technology/10-hot-internet-of-things-startups.html
- https://www.i-scoop.eu/internet-of-things-guide/internet-things-project-failure-success/
- https://www.postscapes.com/closed-iot-companies/

Matthias Fischbacher found:    
https://www.cio.com/article/2602467/consumer-technology/10-hot-internet-of-things-startups.html   

## Three successful business ideas:  

### Big Belly
http://bigbelly.com/ => Smart City Solutions    
Create products like a trash compactor bin which sends notifications to the right officials when it is full

### August
http://august.com/ => Smart Locks    
Remotely lock and unlock doors in your home, keyless entry for family and friends.  

### Adhere TEch
https://adheretech.com/ => Smart wireless pill bottles     
If a patient does not take his/her pills, a notification or alert gets sent to neighbours, family, doctors etc.

### Humavox
http://www.humavox.com/ => Wireless charging of small IoT-devices

## Three failed business ideas:  

### Sigmo
https://www.crowdfundinsider.com/2015/05/67907-where-are-they-now-sigmo-fails-to-talk-understand/     
A voice translating device that promised “at the touch of a button” you would be able to instantly translate the words you need into any chosen language and hear the results of your translation.    
Unfortunately for backers of this project the only thing ever delivered were broken promises and pretty pictures.   
Sigmo is a crowdfunding failure.

### Ninja Blocks
https://www.crunchbase.com/organization/ninja-blocks   
Ninja Blocks is the creator of the Ninja Sphere, the missing brain for your smart devices. Ninja Sphere learns from your behaviour, identifies your preferences, and manages your devices so you don't have to.  

The Ninja Sphere incorporates a gesture interface and micro-location technology that accurately locates radios within the home. It is backwards compatible with the Ninja Block, a popular device and platform that supports 100s of third-party devices.  

Ninja's should be effective and invisible as should the interface to your home   
Failed:   
http://www.linux-magazin.de/NEWS/Ninja-Blocks-Projekt-ist-pleite   
Because getting broke while producing devices.

## Narative
https://www.crunchbase.com/organization/narrative    
Narrative provides hardware and software tools for automatic visual storytelling and known for the wearable camera narrative clip.   
=>http://getnarrative.com/

## Zeo
https://www.crunchbase.com/organization/zeo    
Zeo manufactures sleep monitors that record electrical activity along the scalp and monitor the user's brain waves.    
Failed: http://www.mobihealthnews.com/22484/take-positive-lessons-from-zeo-failure     
The main problem was the business model, with a suboptimal profit margin; also, users didn't care much about accuracy, and competing devices like the FitBit, based on (less accurate) actigraphy, were apparently "good enough" while also performing other functions like monitoring physical activity.

Note for me: Sensoria is doing nice stuff!!!

## Michael Schloh von Bennewith
### Domains influencing:
- IoT Security   
- IoT development working under the mandate of the Intel Innovator program   
- Connected devices  
### Projects involved:
- The Tor Browser  
- Terminal software for Windows  
- Server technology for telecom
- https://www.wolfssl.com/
### Current Project
- IoT development working under the mandate of the Intel Innovator program (promote OpenCV)
- Browser development for Tor and Mozilla including FirefoxOS engineering (The Tor Browser)  
- Hardware wallet for monero (smiliar to BitCoin)  
- Amazon AWS Internet of Things Solution  
### Impact on startups  
Mandate of the Intel Innovator program
### Question
How can we make all our smart home stuff secure, any good framework suggestions to totally encapsulate it?   
What exactly did you do for the Tor Project?   

December => Monero, privacy conference  
https://www.rambus.com/blogs/what-is-a-secure-element/    
https://freeswitch.org/    
