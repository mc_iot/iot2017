# Exercise 11

In exercise 11 I played with the Reed-Switch and built a module to control the current status of the toilet door (needed for the Project 3 app). I programmed it using Arduino IDE as well as by using UlnoIoT.
For me the usage with UlnoIoT was way more comfortable because the MQTT support is build into the framework and easy to use compared to the Arduino IDE. There you have to download a dedicated MQTT library to get this working which is way more time consuming, especially with the network being very slow sometimes in the FH.

(I would have put a picture in here if one of my colleagues from the project 3 would have sent it to me but the one unfortunately didn't)
