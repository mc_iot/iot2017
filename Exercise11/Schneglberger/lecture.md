#Participation Proof 11 Martin Schneglberger
##Tamar Aslan
### Domains influencing:
- Art in combination with electronics
- Design
- Internet of Things
### Projects involved:
- Cuor (See/feel heartbeat of beloved ones on device)
- Flame (interactive fire sculpture)
- NOX (pull switch to turn facade of Ars Electronica on and off)
### Current Project
- Creative Engineer, Designer and Researcher
- City Games Vienna (creating Urban Games)
### Impact on startups  
- Show that creativeness & art is necessary for IoT projects
- Gives vision
### Question
How did you do the switch from coming from a design background to integrate technical stuff? Was it hard to learn all these things from the beginning on?  
Have you thought of kickstarter or indiegogo?

#IMPORTANT
Biggest failue you can do when doing a startup are not on the software but on the people&communication level

##IoT companies

Other companies done by Dominik Reichinger and Michael Rockenschaub

### Intel (Reichinger)
+ Smart buildings, energy, healthcare, smart cities, automated driving
+ Have the advantage, that they have lots of guys with lot of hardware experience
### Resin.io (Reichinger)
+ IoT containers
+ Develop iteratively, deploy safely, and manage at scale.
+ Going to release an open-source version of resin.io soon so you can host them on your own server
+ Yes because they have a good business model that scales well with IoT (pay per used device)
### Sensoro (Reichinger)
+ Provide a suite to easily manage IoT devices with sensors
+ Alpha platform to support 120 square miles of connected devices
+ Yes because they already have good partners (google microsoft etc.) and ["link"](https://sociable.co/technology/exploding-iot-sensoro/)
### Seeed (Reichinger)
+ Selling components for the IoT
+ Specialised in IoT, also selling whole starter kits   
+ Useful if one wants to start with IoT but does not yet know where to get all his stuff from
+ China stores can have high delivery times/prices, so this might be an good alternative

### Runtastic (Rockenschaub)
Runtastic is mainly a sports-platform which has started with tracking a person when running with their smartphone. This can be evaluated either in an app or an online application, where it comes to a lot of data, even from sensors like for example a heart rate sensor.   
This brought runtastic to the domains of sports, health and social, since you can compete with your friends on the platform.

### Pebble (Rockenschaub)
Pebble developed a line of smartwatches including the first commercial successful smartwatch, which used a customized version of FreeRTOS and could communicate with Android and iOS apps over bluetooth.  
Unfortunately it got discontinued after the buy out through fitbit.

### Nest (Rockenschaub)
Nest develops different kinds of IoT gadgets like thermometers, outdoor or indoor cameras - so it covers lifestyle and security.  Nest got acquired by Google.

### Adafruit (done by me)
####Domains
- Provide quite cheap hardware to make creating IoT devices available for everybody
- Provides guides for this hardware do learn how to work with these devices
####History
Limor Fried began selling electronic kits on her website from her own designs in 2005 as a student. She later found Adafruit Industries in New York. In 2010, Adafruit offered a US$1,000 reward for whoever could hack Microsoft's Kinect to make its motion sensing capabilities available for use for other projects. This reward was increased to $2000 and then $3000 following Microsoft's concerns about tampering. In 2013, the company had $22 million in revenue; for 2014 increased to $33 million.
####Result
They are duing a good job, ship internationally and are one of the main distributor of the Raspberry PI    
For example, the NeoLight strip we are using is created by Adafruit

### Sparkfun(done by me)
####Domains
- Provide quite cheap hardware to make creating IoT devices available for everybody
- Provides guides for this hardware do learn how to work with these devices
####History
SparkFun Electronics was founded in 2003 by Nathan Seidle when he was a Junior at University of Colorado Boulder. Its first products were Olimex printed circuit boards. The name 'SparkFun' came about because one of the founders of SparkFun was testing a development board, and sparks flew out; Fun was chosen because the company's self-stated aim is to educate people about electronics. In January 2011, an education department was formed to outreach to local schools, hackerspaces, and events.
####Result
SparkFun has become one of the favoured suppliers for those without mainstream suppliers as well as the increasingly popular "Maker" community, particularly for the Arduino and related devices.

### Element14 (done by me)
####Domains
- element14 Design & Manufacturing Service
- manufacturer of the world's most popular single board computers, like Raspberry Pi, BeagleBone Black and BBC micro:bit
- provide hardware components (also smd)
####History
The company, which was established as the European arm of Premier Farnell, introduced the element14 brand name in 2010, replacing the legacy brands of Premier Electronics, Farnell and Farnell-Newark with Farnell element14 and Newark element14
####Result
Premier Farnell stocks 600,000 products across its regional warehouses. It also sells about half of the Raspberry Pi computers distributed worldwide
