#Lab 11
Today, I integrated the MQTT funtionality into my shower control unit.    
I wanted to do this part in the lecture because it was easy to communicate with my colleagues as we all were in one room (and therefore we could agree on the topics etc.)   

I used this library for achieving the MQTT functionality: [library](http://pubsubclient.knolleary.net)   
Most of the time I was searching an error in the following code (simplified to the important basics):

```
void setup() {
  //....
  client.setServer(mqtt_server, 1883);
  client.setCallback(mqtt_message_received);
  //....
}

void setup_wifi() {
  //Connect to WiFi
  //....
}

void mqtt_message_received(char* topic, byte* payload, unsigned int length) {
  //...Note: this code only shows what happens when the USERS_RECEIVED_TOPIC is received!

  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  //....Process the received data

  splitStrToArray();
  usersSet = true;

}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ShowerClient")) {
      client.subscribe(USER_TOPIC);
      client.publish(REQUEST_USER_TOPIC, "sendUsersPls");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  //....
}
void loop() {

  if (!client.connected()) {
    reconnect(); //Connect to broker
  }
  client.loop();
  if(usersSet){
    if(!countDownRunning){
      readJoystick();
    } else if(millis() - startShowerTimestamp > selectedTime*1000) { //TODO: SEC TO MIN HERE
      timeIsUp();
    } else {
      showRemainingTimeWithStrip();
    }
  }
}
```


As you can see, I set the *mqtt_message_received()* as the callback of the MQTT client. As soon as a valid connectoin to the MQTT server is established in the *reconnect()* function, the message "sendUsersPls" gets published. The logic Patrick implemented in NodeRed now answers with a string containing the name of all users of the desired flat, seperated with a pipe. According to the console, this was actually the case, and the broker also got by message, which means that I was really successfully connected to the right server. Nevertheless, the callback function never got called - even when I removed all the logic which decided what to do with the various topics (therefore: It was not called, no matter what topic I subscribed to and to which topic a message was published). I also thought that maybe I publish the request message too soon after establishing the connection and therefore published it in a loop until the callback is called. The result was that our MQTT broker freezed and I still my callback funtion was not called. Also, this could not be the error, as we saw my request on the Raspberry Pi even if I sent it one time (after the successful connection - just as in the code above).   
When testing with the example mqtt project from the library I used ([library](http://pubsubclient.knolleary.net)), I was able to receive the messages just as I wanted to. Therefore the error had to be in my code. I tried moving the callback function to different places as well as subscribing to the topics on multiple lines (and severaly functions), but did not find any mistake.     
**Solution:** After nearly two hours I realized that my *mqtt_message_received()* function is called *callback* in the example code. I did not believe that this would be my "mistake", but after I named the function the same way as they do in the example, I got the messages in my project... YEEEJJJ....Well....
