# Lecture notes - 30.10.2017

## Presentations

### Mobile Car drinking Challenge

- remote controlled car (by App)  

|  Subject   | Score |
|------------|:-----:|
|presentation|   4   |
|originality |   4   |
|quality     |   4   |

Note: High quality video production

### LORA the Explorer

- IoT project for shools

|  Subject   | Score |
|------------|:-----:|
|presentation|   3   |
|originality |   4   |
|quality     |   4   |

Note: Nice fancy web dashboard, really cool idea

### Alternative's home automation

|  Subject   | Score |
|------------|:-----:|
|presentation|   3   |
|originality |   3   |
|quality     |   3   |

Note: What does your project's name mean?

### Cool Room automation

- controlling light/other mechanisms of a cooling room automatically

|  Subject   | Score |
|------------|:-----:|
|presentation|   3   |
|originality |   4   |
|quality     |   4   |

Note: Cool Video (Lego!)

### Biomat

- Hangover prevention device network

|  Subject   | Score |
|------------|:-----:|
|presentation|   4   |
|originality |   4   |
|quality     |   4   |

Note: Brilliant playthrough! How much

### Smart Home Party

- party management with IoT

|  Subject   | Score |
|------------|:-----:|
|presentation|   4   |
|originality |   4   |
|quality     |   4   |

Note: Awesome scenario!

##Ranking

1. Smart Home Party
2. Mobile Car drinking Challenge
