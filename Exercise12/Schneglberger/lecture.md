# Lecture 12 - Martin Schneglberger

##Rating the presentations
- presentation
- originality
- quality

Write down one question, praise, or critique.

###Presentation 2 (Drinking Car game)

- Presentation:  4
  - Nice Presentation
  - Good quailty, seemed like they used expensive equipment
- Originality: 4
  - I really like the project idea - sounds fun!
  - Could maybe also be realized with bluetooth, but hey, IoT is cool and fun!
- Quaility: 4
  - You did a good job

Who of you edited the video?

##Presentation 3 (Weather Station (LoraTheExplorer))

- Presentation:  2
  - Nice Presentation, but maybe a little bit to long
- Originality: 3
  - Weather station itself is nothing new, but creating a IoT system and use LoRa is a nice idea and fancy
- Quaility: 4
  - Seems like the technical stuff was a lot of work

  How much work was it to set up the LoRa things? Where is the next gateway?

##Presentation 4 (Home Automation Project)

- Presentation:  3
  - Beautiful layout/design, everone spoke really good...demo vides were sometimes a little bit hard
- Originality: 3
  - You had some nice ideas (like gas sensor in a wine cellar)...I liked these specific use cases
- Quaility: 4
  - Nice setup, many components, you did a good job

Which kind of gas can the gas sensor determine? Can the air quailty itself be measured?

##Presentation 5 (Kühlraum Project)

- Presentation:  3
  - Bascially nice presentation, but maybe do not use dialect for audio
- Originality: 3
  - Nice idea, not the usual "home automation" use cases...makes sense in the industry for sure
- Quaility: 3
  - Solid implementation, nice!

Where did this idea come from?

##Presentation 6 (Beeromat)

- Presentation:  3
  - Scene play was a bit hard to follow
- Originality: 3
  - Nice idea, perfect for students
- Quaility: 3
  - App looks very good, steering rolos is very nice

How do you measure how many beers I am still able to drink?

##Presentation 7 (Smart Home Party)

- Presentation:  3
  - Your video was fun! But presentation (in sense of slides) was not that fancy
- Originality: 4
  - Not a classical smart home project, I like how you use student shared flats as the target audience (and you really nailed that!)
- Quaility: 4
  - Nice concept, nice implementation!

How accurate is the alcohol sensor?
