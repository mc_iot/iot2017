#Portfolio Index Martin Schneglberger

The first day we had a little introduction to the Internet of Things in the lecture. We also got some basic information about this course.   
In the lab we got our hardware and setup the raspberry pi based on the unoiot image. We created a little blink program for the ESP8266. All the parts were done together, expect the last step (Trigger blink remotely) => This was done by me.    
Lecture first day: [Exercise01/Schneglberger/lecture.md](Exercise01/Schneglberger/lecture.md)     
Lab first day: [Exercise01/Schneglberger/portfolio.md](Exercise01/Schneglberger/portfolio.md)     
Source code: [Exercise01/Workspace](Exercise01/Workspace)  

The second day we learned about various bus systems and story driven modeling / scenarios in the lecture.   
In the lab we made a button device which connects to a IFTTT WebHook which triggers a notification on our phones.    
I did the programming together with Michael. Additionally, I set up the WebHook.   
Lecture second day: [Exercise02/Schneglberger/lecture.md](Exercise02/Schneglberger/lecture.md)    
Lab second day: [Exercise02/Schneglberger/portfolio.md](Exercise02/Schneglberger/portfolio.md)    
Source code: [Exercise02/Workspace](Exercise02/Workspace)    

The third day was about exchange formats and important Foundations/Alliances/Consortiums in the IoT area.    
In the lab we did a simple COAP Arduino example (with the use of copper) and built a MQTT temperature and AC simulator.   
Lecture third day: [Exercise03/Schneglberger/lecture.md](Exercise03/Schneglberger/lecture.md)   
Lab third day: [Exercise03/Schneglberger/portfolio.md](Exercise03/Schneglberger/portfolio.md)    
Source code: [Exercise03/Workspace](Exercise03/Workspace)   

The fourth day was all about scaling, testing and IoT frameworks.   
In the exercise, we took deeper insight into NodeRed and MongooseOS by reusing the temperature simulators from the previous exercise and used Mongoose to read and write GPIO and combining it with NodeRed.   
We did all of the work together.    
Lecture fourth day: [Exercise04/Schneglberger/lecture.md](Exercise04/Schneglberger/lecture.md)  
Lab fourth day: [Exercise04/Schneglberger/portfolio.md](Exercise04/Schneglberger/portfolio.md)  
Source code: [Exercise04/Workspace](Exercise04/Workspace)

On the fifth day, we wrote down everything we want to see in an IoT framework and got a first insight into UlnoIOT!   
In the lab we started to rebuild project 01 with UlnoIoT together.   
Lecture fifth day: [Exercise05/Schneglberger/lecture.md](Exercise05/Schneglberger/lecture.md)  
Lab fifth day: [Exercise05/Schneglberger/portfolio.md](Exercise05/Schneglberger/exercise05.md)  
Source code: [Exercise05/Workspace](Exercise05/Workspace/)

The sixth day we got the requirements for the second project in the lecture. Additionally, we got another live demo of UlnoIOT and had to define five questions.  
In the lab we continued working with ulnoiot and demonstrated our first project. Unfortunately we had problems with the LED-Strip, which we could solve with help of our professor
Lecture sixth day: [Exercise06/Schneglberger/lecture.md](Exercise06/Schneglberger/lecture.md)  
Labo sixth day: [Exercise06/Schneglberger/portfolio.md](Exercise06/Schneglberger/portfolio.md)  
Source code: [Exercise06/Workspace](Exercise06/Workspace)

On the seventh day, we learned which things do not work when talking about the Internet of (Broken) Things in the lecture.     
In the lab, we set up the scale and the ulnoiot I2C arduino connector.
Lecture seventh day: [Exercise07/Schneglberger/lecture.md](Exercise07/Schneglberger/lecture.md)    
Lab seventh day: [Exercise07/Schneglberger/portfolio.md](Exercise07/Schneglberger/portfolio.md)  
Source code: [Exercise07/Workspace](Exercise07/workspace)

The eight day was about security measurements against the Internet of Broken Things, LoRa and TNN.  
In the exercise, we continued with the work from the previous laboratory and started with the second project.    
Lecture eight day: [Exercise08/Schneglberger/lecture.md](Exercise08/Schneglberger/lecture.md)   
Lab eight day: [Exercise08/Schneglberger/portfolio.md](Exercise08/Schneglberger/portfolio.md)   
Source code: [Exercise08/Workspace](Exercise08/Workspace)

The ninth lecture was about IoT maintenance, project three and data visualization.  
In the lab, we showed our professor the outcome of our second project and had a long discussion about it. Additionally, we collected ideas for the third project and designed some scenarios.  
Lecture ninth day: [Exercise09/Schneglberger/lecture.md](Exercise09/Schneglberger/lecture.md)   
Lab ninth day: [Exercise09/Schneglberger/portfolio.md](Exercise09/Schneglberger/portfolio.md)  

On the tenth session we learned about IoT business opportunities and had a remote talk with Michael Schloh von Bennewitz
In the lab we started with the third project!    
Lecture tenth day: [Exercise10/Schneglberger/lecture.md](Exercise10/Schneglberger/lecture.md)    
Lab tenth day: [Exercise10/Schneglberger/portfolio.md](Exercise10/Schneglberger/portfolio.md)  

On the eleventh day we researched about IoT success stories and had a remote talk with Tamer Aslaan.    
In the lab we continued working on our third project.   
Lecture eleventh day: [Exercise11/Schneglberger/lecture.md](Exercise11/Schneglberger/lecture.md)   
Lab eleventh day: [Exercise11/Schneglberger/portfolio.md](Exercise11/Schneglberger/portfolio.md)   

Finally - Presentation day of the third project!     
Lecture twelfth day: [Exercise12/Schneglberger/lecture.md](Exercise12/Schneglberger/lecture.md)

Project 1: [Project01/Schneglberger/portfolio.md](Project01/Schneglberger/portfolio.md)    
Source code: [Project01/Workspace](Project01/workspace)

Project 2: [Project02/Schneglberger/portfolio.md](Project02/Schneglberger/portfolio.md)   
Source code: [Project02/Workspace](Project02/workspace)

Project 3: [Project03/Schneglberger/portfolio.md](Project03/Schneglberger/portfolio.md)    
Source code: [Project03/Workspace](Project03/Workspace)

Total Project 03 documentation:   
[Project03](https://bitbucket.org/mattfisch34/iot_course_team/src/5351286971057fe7dbe49d4451ddf9bc9743ab8e/projects/project3/?at=master)
