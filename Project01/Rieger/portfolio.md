# Project 1 Portfolio Michael Rieger

- Built the Node-RED integrator without the function of the reset button (reset button was done by Martin)
- Node-Red contains all of the actual business logic (integrator)
- Install and use Mongoose OS
  - After installing it on my Arch Linux Mongoose OS always crashed after startup. Since it still forwarded me to the Mongoose OS Web frontend i didn't notice that it died right after the start-up. After looking to the console I saw that it throws a random go error. I tried 20 minutes to get it running but then switched to a Windows 10 PC
  - After installing Mongoose OS on Win10 i downloaded the ESP8266 driver from the internet and connected my Wemos to Mongoose OS
  - After the initial flash I often got the problem that my Wemos was hanging in a bootloop. Matthias Fischbacher told me then that the intial config ("\*\*0.conf") may not be overwritten since the config file derive and overwrite each other

We used:
- Node-Red for the business logic (integrator)
- Mongoose to access the LED Strip and the button and handle publishing/subscribing relevant topics for these components
- Java (IDE: IntelliJ) to program the Temperature simulator as well as the temperature border
- IFTTT to create a webhook which sends notifications on temperature change

##Story / Scenario (done by Martin)

Residents: Michael, Dominik and Martin

Michael, Dominik and Martin have a shared flat in Hagenberg.  
Since they all study Mobile Computing and are very smart, they decided to automate their air condition in order to not waste valuable coding-time by playing around with such primitive devices.  
It is final time, their brains are running hot and therefore they need a cooler climate in their flat. To achieve this, Michael uses the **Potentiometer** to adjust the desired temperature. Immediately, Dominik and Martin get a **notification** for this incidence on their smartphones.  
The **temperature senors** notice that it is to warm in the flat and send a command to the air condition to cool down. The **WS2812 RGB Strip** display this state by setting all LED colors to ice blue.  
As soon as the desired temperature is reached, the LEDs shine green.
After Martin wakes up, he notices that it got to cold, so he uses the poti to increase the desired temperature again. Due to the fact that the AC is heating up the flat now, the LED strip shines red.  
Finals are over now, Dominik wants to set the flat temperature back to normal again, so he presses the **reset-button** in order to restore the defaults again. The LED strip turn to a red chasing light to acknowledge this process.
