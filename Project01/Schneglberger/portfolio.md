#Project 1 Portfolio Martin Schneglberger

Source code: [Project01/Workspace](../workspace)

- Import Node-Red state of Michael via the clipboard - import feature as JSON to develop further
- Node-Red contains all of the actual business logic (integrator)
- Open Mongoose OS
  - **First problem**: I wasted like a half hour due to the fact that Mongoose OS could not connect to the ESP8266.
  - Either ESP8266 is broken, or it was just coincidence
  - **Second problem**: Sounds weird, but the second ESP8266 caused by laptop to make random mouseclicks and could not be flashed
  - Used another ESP8266
  - **Thrid problem**: After I programmed a bit I always had to restart Mongoose multiple time since the application always freezed itself. This caused the waste of another 45 minutes.
  - **Fourth problem**: I wanted to display a chasing light when the reset button gets pressed. This chasing light should not be "overwritten" with *coolDown* or *heatUp* events. Therefore I thought it is a good idea to create a timestamp on button press and only cancel the chasing light timer after a certain period of time. Unfortunatelly, creating Timestamps with e.g `(new Date()).getMilliseconds())` caused the program not to work anymore.
  I had to create a counter in the subscribe-function instead which resets after a certaion amount of calls. This is kind of dirty, as the duration of the chasing light now depends on the amount of incoming events.
- Using Node-Red to implement the business logic was quite easy and straight forward. Also using it to send HTTP-Requests to IFTTT could be achieved quite fast.

We used:

- Node-Red for the business logic (integrator)
- Mongoose to access the LED Strip and the button and handle publishing/subscribing relevant topics for these components
- Java (IDE: Intellij) to program a Potentiometer and Temperature simulator with a mqtt library: [library](https://github.com/fusesource/mqtt-client)
- IFTTT to create a webhook which sends notifications on temperature change

![alt text](./node-red.PNG)

- **temp-adjust-in**: Receives the events triggered by potentiometer changes, sets the new *tempborder* and triggers an IFTTT notification
- **temp-in**: Receives events triggered by the temperature sensor and publishes a event telling the AC what to do ('coolDown', 'heatUp', 'perfect')
- **reset**: Receives reset events triggered by the reset button. Sets the *tempborder* value back to 21 degrees and trigger an IFTTT notification

##Story / Scenario

Residents: Michael, Dominik and Martin

Michael, Dominik and Martin have a shared flat in Hagenberg.   
Since they all study Mobile Computing and are very smart, they decided to automate their air condition in order to not waste valuable coding-time by playing around with such primitive devices.   
It is final time, their brains are running hot and therefore they need a cooler climate in their flat. To achieve this, Michael uses the **Potentiometer** to adjust the desired temperature. Immediately, Dominik and Martin get a **notification** for this incidence on their smartphones.   
The **temperature senors** notice that it is to warm in the flat and send a command to the air condition to cool down. The **WS2812 RGB Strip** display this state by setting all LED colors to ice blue.   
As soon as the desired temperature is reached, the LEDs shine green.
After Martin wakes up, he notices that it got to cold, so he uses the poti to increase the desired temperature again. Due to the fact that the AC is heating up the flat now, the LED strip shines red.   
Finals are over now, Dominik wants to set the flat temperature back to normal again, so he presses the **reset-button** in order to restore the defaults again. The LED strip turn to a red chasing light to acknowledge this process.

##My part:

  - Programming the two simulators, one for the temperature and one for the potiometer
  - Reset functionality with the button
  - Node Red scenario which pubishes the right commands for the AC according to the desired temperature
  - With the ESP-8266 I subscribed to this MQTT Topic and react to the commands with the WS2812 RGB Strip
    - When the temperature value is perfect, show green colors
    - When the temperature value is to warm, show ice-blue colors
    - When the temperature value is to cold, show read colors
    - When the reset button gets pressed, show a red chaser light
    - Michael was doing all the initial work to make controlling the LED Strip with Mongoose possible, which was a lot.
- Writing the story
- Creating the webhook which sends notifciations on temperature change
