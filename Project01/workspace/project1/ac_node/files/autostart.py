# This is the autostart code for a ulnoiot node.
# Configure your devices, sensors and local interaction here.

# Always start with this to make everything from ulnoiot available.
# Therefore, do not delete the following line.
from ulnoiot import *
from ulnoiot import _rgb_animator

r = rgb_multi("acstrip", d1, 8)

## Start to transmit every 10 seconds (or when status changed).
# Don't forget (uncomment!) the run-comamnd at the end.
run(10)
