# Project 2 portfolio (portfolio nearly entirely done by Martin)

## HX711

My bottle has a weight of **178g**
We added water(**53g**) so that we had a total weight of **231g**
Shaking the whole scale resulted in a weight difference of +/- 2g  

Weight sensor has to be beneath the tank (or the whole tank has to be placed on weight sensors).  
This is the same for every type of liquid (expect a tank filled with fuel will be heavier)

## Super-sonic HC-SR04

Pin-layout:
- White: +5V  
- Brown: GND  
- Red: D7  Echo
- Black: D6  Trigger

When holding it above a 3/4 full water bottle (non-transparent, aluminium)   
Reflected in a way that it measured 3cm to the waterlevel, which is quite accurate (when considering that the sensor was a little bit inside the bottle).  

Notice: Even though the datasheet states a maximum measuring distance of 3m, we were only able to get accurate values to ~220cm.   
Sometimes the sensor put some invalid/inaccurate values in between. This was especially the case when we tried to measure the distance to the ceiling (continously 210cm, but sometimes 164cm)  

# VL53L0X

=>http://www.st.com/content/ccc/resource/technical/document/datasheet/group3/b2/1e/33/77/c6/92/47/6b/DM00279086/files/DM00279086.pdf/jcr:content/translations/en.DM00279086.pdf

Operating power: 3.3V

Problem: We forgot to remove the little cover of the light sensor which cost us a little bit of time.

Even with clear water in a non-transparent bottle (aluminium), the result was ranging between 44-70mm (therefore the average would be quite accurate). Nevertheless, sometimes some huge mistakes (+7cm) could be found in the Serial output.

#Sunfounder Raindrop sensor (Done by Martin - me)

Operating Power: 5V
Pin-Out:  
- A0 (Analog - white cable) to Pin 2 (ADC0)
- VCC (Power - red cable) to 5V
- GND (Ground - black cable) to GND
- D0 (Data - brown cable) to Pin 17 (D4)   

**Problem**   
Always either 1023 or 0 (ADC0)...No steps in between
