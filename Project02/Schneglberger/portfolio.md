#Project 02 - Martin Schneglberger

All source code used for this project: [Project02/Workspace](../workspace)

##HX711

My bottle has a weight of **178g**    
We added water(**53g**) so that we had a total weight of **231g**    
Shaking the whole scale resulted in a weight difference of +/- 2g  

Weight sensor has to be beneath the tank (or the whole tank has to be placed on weight sensors).    
**This is the same for every type of liquid (expect a tank filled with fuel will be heavier)**

![alt-text](./scale-results.png)

##Super-sonic HC-SR04

Pin-layout:

- White: +5V  
- Brown: GND  
- Red: D7  Echo
- Black: D6  Trigger


![alt-text](./sonic.png)

When holding it above a 3/4 full (clear) water bottle (non-transparent, aluminium)   
Reflected in a way that it measured 3cm to the waterlevel, which is quite accurate (when considering that the sensor was a little bit inside the bottle).  

![alt-text](./sonic_plotter.png)

Notice: Even though the datasheet states a maximum measuring distance of 3m, we were only able to get accurate values to ~220cm.     
Sometimes the sensor put some invalid/inaccurate values in between. This was especially the case when we tried to measure the distance to the ceiling (continously 210cm, but sometimes 164cm)  

The results had minor deviations between the different water/oil types, but as with the VL53L0X some of this difference may have the reason in not accurate measurement methods (holding the sensor with free hands).    
But just as a note, the largest distance was measured when using oil, followed by dirt water and normal water.  

We borrowed a the cardboard with two holes in it encapsulate the whole bottle.

#VL53L0X

=>[datasheet](http://www.st.com/content/ccc/resource/technical/document/datasheet/group3/b2/1e/33/77/c6/92/47/6b/DM00279086/files/DM00279086.pdf/jcr:content/translations/en.DM00279086.pdf)

Operating power: 3.3V

**Mistake: We forgot to remove the little cover of the light sensor (Damn it)**

Connection:  
![alt-text](./light.png)

Output:  
![alt-text](./vl53l0x_plotter.png)

Even with clear water in a non-transparent bottle (aluminium), the result was ranging between 44-70mm (therefore the average would be quite accurate). Nevertheless, sometimes some huge mistakes (+7cm) could be found in the Serial output  

The results were the same with clear and dirt water, as well as with oil water.   
**Note**: Of course there were some differences in the range of 1 cm, but this might is due to the fact that we hold the sensor with our hand and therefore might have inaccurate values.    
Nevertheless we got other distances than with the super-sonic sensor, but this is due to the fact that the actual sonic sensors reached about one centimeter into the bottle.  

#Sunfounder Raindrop sensor

Operating Power: 5V
Pin-Out:  

- A0 (Analog - white cable) to Pin 2 (ADC0)
- VCC (Power - red cable) to 5V
- GND (Ground - black cable) to GND
- D0 (Data - brown cable) to Pin 17 (D4)   

![alt-text](./water.png)      

**Problem**

Always either 1023 or 0 (ADC0)...No steps in between

**Solution**:  
```
const int analogPin=A0; //this gave us the right values
const int analogPin=2; //this gave us the wrong values

```
This is really odd, since they both should refere to the same pin:

![alt-text](./esp8266-pinout.png)

Right values (some clear water drops on the sensor)

![alt-text](./raindrop_ plotter.png)

With oil, this did not work at all! Even when we put quite much oil onto it, the values did not change:

![alt-text](./raindrop_oil.png)

With dirt water, we did not get any other results as with clear water.

#Reed Switch

A Reed Switch could be used to determine when the cap of a tank containing any liquid gets opened or closed.  
Hence, the locking meachism would need contain something magnetic. This sensor could be helpful in order to determine when liquids are currently added or to stop all machines when there are maintenance tasks going on.  

#Suggestion:

The raindrop sensor would not give any reliable information about the actual water level in the tank, only if there is water or not.  
Only when using multiple raindrop sensors at different levels of the tank we could create a solution which could give "semi-accurate" information about the e.g. remaining waterlevel. (Nevertheless this would be inaccurate when there are high waves)   
**Attention** The waterdrop sensor can not be used for oil! (At least not "corn oil")

The *weight sensor* could give very accurate information about the amount of liquid inside the tank, but the whole tank would have to be placed on a scale.  

The *light distance sensor* works best in a non-transparent tank with all liquids, but we would use the sonic distance since we got better results (when using the components we have, the super sonic sensor has a higher maximal distance etc.). We suggest mounting multiple sonic distance sensors to also give accurate information when the tank is not completely horizontal.  

A *Reed Switch* could be used to determine when the cap of a tank containing any liquid gets opened or closed. This can be helpful to e.g. stop motors when the cap gets openend.  

*LoRa* sounds like a create technology to take remote insight into the state of your boat.  

As a feature and to pimp up your boat we would suggest to mount an IP68 water resistent LED strip on the sides of your boat. So your boat can glow very fancy in the dark. When combining with a on/off button, you can also morse in an emergency case.  

For statistic purposes, a *gyroscope* would be nice. Imagine getting information about the highest angle your boat had when hitting that one big wave!  

**Attempts:**  
We thought about using a barometer to determine the water level. For this we would have to use it as a "swimmer" floating on top of the water level.
But as we took insight into the datasheet, we found out that the height difference in a tank resulting when consuming e.g. oil is too low to give accuarate feedback.  
The reed switch can also be used in conjunction with a floating magnet to trigger an alert when the tank is (nearly) empty.  
A hall sensor in conjunction with a floating magnet would therefore be a better idea.
