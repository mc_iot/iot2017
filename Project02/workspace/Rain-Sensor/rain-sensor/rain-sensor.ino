/******************************************************
name:Raindrop Detection
function:drop some water onto the sensor,
When the quantities of the raindrops exceeds the threshold,
the LED on the raindrop sensor module and that hooked up with pin 13 of the SunFounder Uno board light up
*******************************************************/
const int analogPin=A0; //the AO of the module attach to A0
const int digitalPin=17; //D0 attach to pin17
int Astate=0; //store the value of A0
boolean Dstate=0; //store the value of D0
long t;

void setup() 
{
  pinMode(digitalPin,INPUT); //set digitalPin as INPUT
  Serial.begin(9600); //initialize the serial monitor
}
void loop() 
{
  Astate=analogRead(analogPin); //read the value of A0
  if (millis() > t + 250) {
    Serial.print("A0: ");
    Serial.println(Astate); //print the value in the serial monitor
    Dstate=digitalRead(digitalPin); //read the value of D0
    //Serial.print("D0: ");
    //Serial.println(Dstate);
    t=millis();
  }
}
