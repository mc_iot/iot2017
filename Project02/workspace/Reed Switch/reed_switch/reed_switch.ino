const int digitalInPin = 17;// reed switch attach to D4
void setup()
{
  Serial.begin (9600);
  pinMode(digitalInPin,INPUT);// set digitalInPin as INPUT
}
void loop()
{
  boolean stat = digitalRead(digitalInPin);//read the value
  if(stat == HIGH)// if it it HIGH
  { 
    Serial.println("Closed");
  }
  else //else
  {
    Serial.println("Open");
  } 
}

