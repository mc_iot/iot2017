# Project 03

## The Team
- Patrick Felbauer
- Matthias Fischbacher
- Lorenz Graf
- Michael Rieger
- Martin Schneglberger

Picture of us taken from the "How I met your mother" - like outro
![Group picture](./group_pic.jpg)

## My part

I realized the toilet door switch in UlnoIoT and participated with in the implementation of the SSC app, focusing on the backend implementation and UI.

As for the video I voiced the whole story. Was a lot of work but totally worth it!

The documentation can be found on our project 3 repository:
[here](https://bitbucket.org/mattfisch34/iot_course_team/src/5351286971057fe7dbe49d4451ddf9bc9743ab8e/projects/project3/?at=master)

## Presentation

Presentation was done by Martin, Matthias and me:
https://docs.google.com/presentation/d/1xUa65lGY8kK-s7qgtH-Ps1o-JEaQV6T5J1ugAqb8acU/edit
