# This is the autostart code for a ulnoiot node.
# Configure your devices, sensors and local interaction here.

# Always start with this to make everything from ulnoiot available.
# Therefore, do not delete the following line.
from ulnoiot import *

wifi("iotempire-InternetOfBoys", "internetofboys")
mqtt("192.168.12.1", "flatty/toilet")
contact("toilet_switch", d1, "open", "closed")

run(10)
