# Portfolio of Michael Rieger and Martin Schneglberger

## Structure

This repository contains the work and portfolio of the Internet of Things course for MC15.
The repo is structured as follows:

  - each folder represents an exercise
  - those folders contain a subfolder for every individual of this team
  - additionally if there is any source code to share, the code is provided in the "Workspace" directory of each exercise folder
  - the individual portfolio and lecture participation proof of each day are provided in the folder of the individual team member

## Portfolio locations

The portfolio of Michael Rieger can be accessed here:
[Portfolio Rieger](portfolio-Rieger.md)


The portfolio of Martin Schneglberger can be accessed here:
[Portfolio Schneglberger](Martin_Schneglberger.md)  
