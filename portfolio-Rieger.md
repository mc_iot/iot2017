# IoT Portfolio by Michel Rieger

This is the table of contents of my portfolio. Each part of the portfolio is introduced by a little summary and introduction to the specific exercise. The lecture participation proof documents can be found in my seperate folders as an external document.

## Exercise 1

On the first day we got an introduction to IoT. The first day was all about setting up the development environment and tested the hardware by uploading a blink example to the Wemos D1 mini (ESP8266).

Portfolio: [Exercise01/Rieger/portfolio.md](Exercise01/Rieger/portfolio.md)  
Source code: [Exercise01/Workspace](Exercise01/Workspace)  

## Exercise 2

The second day was about protocols.  In the exercise I did the wiring and half of the programming of the hardware part and my colleague did the rest of the source code as well as the **IFTTT** configuration. Additionally I did the communication with the LCD.

Portfolio: [Exercise02/Rieger/portfolio.md](Exercise02/Rieger/portfolio.md)  
Source code: [Exercise02/Workspace](Exercise02/Workspace)  

## Exercise 3

In the third exercise we had to use **CoAP** and **MQTT** to communicate with our devices and send remote commands via the network in order to turn the onboard LED of the Wemos D1 mini on and off (CoAP). The second part of the exercise exists of a program that simulates a temperature sensor and permanently delivers temperature values via MQTT and another program that receives those values.

[Exercise03/Rieger/portfolio.md](Exercise03/Rieger/portfolio.md)  
Source code: [Exercise03/Workspace](Exercise03/Workspace)

## Exercise 4

Day four was all about scalability and testing of IoT devices and IoT frameworks.
In the exercise we had to use **Node-RED** as well as **Mongoose OS** to extend our previously developed temperature simulator with an integrator that receives the temperature value and sends an on or off command for an A/C based on the currently received value. We also got the description for our first project which can be found  [here](Project1).

[Exercise04/Rieger/portfolio.md](Exercise04/Rieger/portfolio.md)  
Source code: [Exercise04/Workspace](Exercise04/Workspace)

## Exercise 5

Exercise 5 was all about the **UlnoIoT** framework. We had to implement our previously developed project 1 in UlnoIoT to get a hands on with this framework.

[Exercise05/Rieger/portfolio.md](Exercise05/Rieger/portfolio.md)  
Source code: [Exercise05/Workspace](Exercise05/Workspace)

## Exercise 6

Exercise 6 was all about project 1 (at least for us) with a nice discussion with UlNo appended and further work with **UlnoIoT**. We continued working on our UlnoIoT verison of project 1 and received the requirements for project 2. Unfortunately we had troubles with our hardware so that cost us a lot of time in the exercise. But with later help of Ulno we got the led strip working.

[Exercise06/Rieger/portfolio.md](Exercise06/Rieger/portfolio.md)  
Source code: [Exercise06/Workspace](Exercise06/Workspace)

## Exercise 7

In this exercise we worked with the **Keyestudio scale KS0087** that uses a **HX711** as load cell sensor. We had to calibrate the scale, configure the scale factor and read the weight of an object placed on it. The weight that has been measured was then received by a Wemos (via I2C) running UlnoIoT and sent to our Raspberry via MQTT.

[Exercise07/Rieger/portfolio.md](Exercise07/Rieger/portfolio.md)  
Source code: [Exercise07/Workspace](Exercise07/Workspace)

## Exercise 8

I continued the setup to install UlnoIoT on my own notebook and resumed our work on on the Keyestudio scale. We finished the task relating the Scale and got UlnoIoT working. So satisfying to open the Ulno projects with PyCharm and code from there.

After this task we continued working on project 2 (see [Project02](/Project02)).

[Exercise08/Rieger/portfolio.md](Exercise08/Rieger/portfolio.md)  
Source code: [Exercise08/Workspace](Exercise08/Workspace)

## Exercise 9

In Exercise 9 we outlined and explained our project 3 idea to UlNo and presented our results of the project 2, following a nice discussion about IoT based boats.

[Exercise09/Rieger/portfolio.md](Exercise09/Rieger/portfolio.md)

## Exercise 10

In Exercise 10 our team were the only ones present, so we let Lorenz give us an introduction to the Ionic framework which we needed for the implementation of our project 3 app.

(No lecture proof on this day since we only had an exercise)

[Exercise10/Rieger/portfolio.md](Exercise10/Rieger/portfolio.md)

## Exercise 11

Exercise 11 was about me playing with the reed switch via Arduino as well as UlnoIoT. Without question I decided to use UlnoIoT because it's just more comfortable since the MQTT support is given from the start and doesn't have to be provided by any external library. I tested different magnets and worked on the app.

[Exercise11/Rieger/portfolio.md](Exercise11/Rieger/portfolio.md)

## Exercise 12

Exercise 12 represents the last lecture we had representing the final presentation day. In this folder the participation proof for this lecture can be found here:
[participation proof](Exercise12/Rieger/2017-11-06.md)

Source Code can be found on the Repo of our project 3 team:
[here](https://bitbucket.org/mattfisch34/iot_course_team/src/5351286971057fe7dbe49d4451ddf9bc9743ab8e/projects/project3/?at=master)
